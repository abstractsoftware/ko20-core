#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QFileInfo>
#include <QIcon>
#include <QtSvg>
#include <QSound>
#include <QQuickView>
#include <QQuickStyle>
#include <QSettings>
#include <QTest>

#include <QMLRender/previewitem.h>

#include <i18n/Translator.h>
#include <O20/StaticAppDefs.h>
#include <Splash/O20Splash.h>
#include <Ink/Ink.h>

int main(int argc, char *argv[]) {
	// Use Material and Breeze styles
        QIcon::setThemeName("Breeze"); // TODO: add option for Papirus IconTheme
	QQuickStyle::setStyle("material");
	qputenv("QT_STYLE_OVERRIDE","breeze");
	qputenv("XCURSOR_THEME","breeze_cursors");
	qputenv("QT_QUICK_CONTROLS_HOVER_ENABLED", "0");
	qputenv("QT_QUICK_CONTROLS_MATERIAL_VARIANT", "Dense");

	qmlRegisterType<PreviewItem>("OF20", 3, 0, "DocumentItem");

	QQuickWindow::setDefaultAlphaBuffer(true);

	// For Retina and HighDPI screens
	QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QApplication o20app(argc, argv);

        QCoreApplication::setOrganizationName("Abstract Software");
        QCoreApplication::setOrganizationDomain("io.gitlab.ko20");
        QCoreApplication::setApplicationName("KO20.Ink");

	QSettings settings;
	settings.beginGroup("UserSettings");
	bool darkMode = settings.value("useDarkMode").toBool();
	qputenv("QT_QUICK_CONTROLS_MATERIAL_THEME", darkMode ? "Dark" : "Light");
//	QIcon::setThemeName(darkMode ? "Breeze" : "Breeze");
	settings.endGroup();

        O20::Translator appTrans;
        appTrans.apply(&o20app);

	QCommandLineParser parser;
	parser.setApplicationDescription("The new KO20 office suite written in QtQuick with a modern MS office design, Google Material controls, floating toolbars, ODT support, syntax highlighting for devs, and more!");

	parser.addHelpOption();
	parser.addVersionOption();

        QCommandLineOption uiDirOption(QStringList() << "ui-dir",
             QCoreApplication::translate("main", "Load UI from QML files in the specified directory."),
             QCoreApplication::translate("main", "directory"));
        parser.addOption(uiDirOption);

	parser.addPositionalArgument("app", QCoreApplication::translate("main", "O2OAppDef = app:ink"));

        QCommandLineOption choose(QStringList() << "o" << "choose-document", QCoreApplication::translate("main", "Opens a file dialog where you can browse avaliable documents. (BUGGY, CAN SOMETIMES LEAD TO CRASHES)"));
        parser.addOption(choose);

	QCommandLineOption documentRO(QStringList() << "r" << "read-only", QCoreApplication::translate("main", "Open document in ReadOnly mode."));
	parser.addOption(documentRO);

	QCommandLineOption createOption(QStringList() << "c" << "create",
        QCoreApplication::translate("main", "Create document if it doesn't already exist."));

	QCommandLineOption textOption(QStringList() << "t" << "text",
        QCoreApplication::translate("main", "Open HTML/XHTML files as plain text"));

	parser.addOption(createOption);
        parser.addOption(textOption);

	parser.addPositionalArgument("document", QCoreApplication::translate("main", "Document to open"));
	parser.process(o20app);

        const QStringList args = parser.positionalArguments();

	bool word = args.size() > 0 && args.at(0) == "app:ink"; //parser.isSet(openWord);
	bool error = !word && args.size() > 0;

        // Breeze palette
        QPalette colorScheme;
        colorScheme.setColor(QPalette::Window, QColor(darkMode ? "#303030" : "#f0f0f0"));
        colorScheme.setColor(QPalette::WindowText, QColor(darkMode ? "#ffffff" : "#414445"));
        colorScheme.setColor(QPalette::Base, QColor(darkMode ? "#101010" : "#fcfcfc"));
        colorScheme.setColor(QPalette::AlternateBase, QColor(darkMode ? "#303030" : "#f0f0f0"));
        colorScheme.setColor(QPalette::ToolTipBase, QColor("#f0f0f0"));
        colorScheme.setColor(QPalette::ToolTipText, Qt::black);
        colorScheme.setColor(QPalette::Text, QColor(darkMode ? "#ffffff" : "#414445"));
        colorScheme.setColor(QPalette::Button, QColor(darkMode ? "#303030" : "#f0f0f0"));
        colorScheme.setColor(QPalette::ButtonText, QColor(darkMode ? "#ffffff" : "#26292a"));
        colorScheme.setColor(QPalette::BrightText, Qt::red);
        colorScheme.setColor(QPalette::Link, QColor("#673AB7"));
        colorScheme.setColor(QPalette::Highlight, QColor(word ? "#2b579a" : "#d83b01"));
        colorScheme.setColor(QPalette::HighlightedText, Qt::white);
        o20app.setPalette(colorScheme);

	KO20::App app;

	if (word) app = KO20::WordApp;

	if (error) app.name = "Error.";

	KO20::Splash splash(app);

	if (!word && !error) splash.makeLargeSplash(), splash.setGalleryMode(true);

	splash.show();

	if (!word) {
		splash.setTip(args.size() > 0 ? QString("Sorry, %1 doesn't exist.").arg(args.at(0)) :
	"Welcome to KO20, a modern and minimal office suite written in QtQuick, with a modern 0365 design.\n\nCopyright (C) 2019-2020 Abstract Developers");
	} else splash.setTip("Loading...");

        QSound::play(":/Sounds/04/ui_loading.wav");

	QTest::qWait(500);
	if (args.size() > 1) splash.setTip("Opening " + QFileInfo(args.at(1)).baseName() + "...");
	QTest::qWait(500);

        // SHOW WINDOW IF LOADING DOCUMENT!!!!!!!!! XXX TODO TODO

	KO20::AppCtl ctrl;
	ctrl.setArgs(args);

	ctrl.ux = parser.value(uiDirOption);
        ctrl.c = parser.isSet(choose);
	ctrl.ro = parser.isSet(documentRO);
	ctrl.to = parser.isSet(textOption);
	ctrl.co = parser.isSet(createOption);

	ctrl.app = word ? "ink" : "";

	splash.kApp = &ctrl;

	if (!splash.launchApp()) return 1;

	return o20app.exec();
}
