#ifndef O20GUI_HEADER
#define O20GUI_HEADER

#include <QMenu>
#include <QMainWindow>
#include <QQuickWidget>
#include <Document/DocumentRender.h>
#include <QMLRender/previewitem.h>

#include <QStringList>
#include <QTextDocument>
#include <QTextCharFormat>
#include <QTextBlockFormat>

#include <odt/converter.h>
#include <Sonnet/SpellCheckDecorator>

#include <QMimeDatabase>

#include <theme.h>
#include <repository.h>
#include <syntaxhighlighter.h>

namespace KO20 {
	struct DocumentInformation {
		QString name;
		QString path;
		QString suffix;

		bool textOnly = false;
		bool open = false;
		bool isReadOnly = false;
		bool isAutosave;
		bool isValid;

		struct Styles {
			QStringList stylenames;
			QMap<QString, QTextCharFormat> charFormats;
			QMap<QString, QTextBlockFormat> blockFormats;
		} styles;
	};

	class Ink : public QQuickWidget {
		Q_OBJECT

	public:
		Ink(QString uiDir = "");
		void startCore();

		void setReadOnly(bool readOnly);
		void setTextOnly(bool textOnly) { m_dInfo.textOnly = true; }

	public Q_SLOTS:
		bool createDocument(QString createTo = "", bool createNoExist = false, QString createFrom = "");
		void createNewDocument();
		void saveDocumentAs();
		void printDocument(bool useDialog = true, QString document = "");

	private Q_SLOTS:

		void aboutQt();
		void onSwipePageChanged(int p);

		void onCursorPositionChanged();
		void onTextChanged();

		void jumpToHeadingPos();

		void liveUpdate(int hovered);
		void editorInterfaceCommands();
		void editorInterfaceParaCommands();
		void editorOddCommands();

		void updatePageBackground();

		void soundSlot();
		void statusBarSlot();
		void manageClipboard();
		void onCheckSpellingToggled();
                void threadedSave();
		void setupDocuments();
		//void resizeDocument();

		void connectStyleSlots();
		void openRecentFile();

		void onFullScreenRequest();

		void setupDirectoryTree();

		void updateStyles();

		void renameDocument();
		void openLocation();
		void closeDocument();
		void trashDocument();
		void setWindowTitle(QString t = "");

	protected:
		//void resizeEvent(QResizeEvent* event);
		void closeEvent(QCloseEvent* event);

	private:
		KO20::DocumentInformation getIoFile(bool openOrSave, QString filters = "");
                void saveStyles(OOO::StyleInformation* o);

		QString icon_for_filename(const QString &filename);

		void generateSCheckObject();
		void deleteSCheckObject();

		QStringList getMimetypes(QStringList list);
		void addRecentFile(QString filename);
		void updateRecentFiles();

		void officeIo(int mode, QString path = "");

                void updateTextToStyles();

		void readSettings();
		void writeSettings();

		KO20::DocumentInformation m_dInfo;
		DocumentRender* m_document;
		QTextDocument* m_textDocument;

		bool m_documentVisible = false;
		bool m_saveLock    = false;
		bool m_commandLock = false;
		Sonnet::SpellCheckDecorator* m_documentDecorator;

		QString currentDir;
		QStringList recentFilesList;

		QList<int> headingPos;

		struct Live {
			bool bold = false;
		} m_liveUpdate;

		QMimeDatabase mime_database;

		bool justCreated = false;

		PreviewItem* documentItem;
		KSyntaxHighlighting::Repository m_repository;
		KSyntaxHighlighting::SyntaxHighlighter *m_highlighter;
	};

};

#endif
