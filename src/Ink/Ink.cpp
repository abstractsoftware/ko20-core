#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QMessageBox>
#include <QSoundEffect>

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include <QSound>
#include <QQuickTextDocument>
#include <QQuickItem>

#include <O20/QtQuick.h>
#include <O20/QtQuickSearch.h>

#include "Ink.h"

#include <definition.h>
#include <odt/converter.h>
#include <docx/openDocx.h>
#include <odt.patch/odtpatch.h>

#define QTDOCUMENTSLOCATION QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
#define QTHOMELOCATION      QStandardPaths::writableLocation(QStandardPaths::HomeLocation)

// this is really bad
#define m_textDocument m_document->document()

namespace KO20 {
    
Ink::Ink(QString uiDir)
{
	setWindowIcon(QIcon(":/O20Core/apps/io.gitlab.ko20.ink.svg"));

	quickWindow()->setTextRenderType(QQuickWindow::QtTextRendering);

	QFont f; f.setFamily("Open Sans"); f.setPointSize(10);
	qApp->setFont(f);

	m_dInfo.open = false;
	setSource(QUrl(uiDir.isEmpty() ? "qrc:/QtQuick/O20/io.gitlab.ko20.ink.qml" : QDir(uiDir).filePath("io.gitlab.ko20.ink.qml")));
	setResizeMode(QQuickWidget::SizeRootObjectToView);

        setWindowTitle();

	rootObject()->setProperty("aboutQtTxt", QString("KDE is a world-wide community of software engineers, artists, writers, translators and creators who are committed to Free Software development. KDE produces the Plasma desktop environment, hundreds of applications, and the many software libraries that support them.\n\nKDE is a cooperative enterprise: no single entity controls its direction or products. Instead, we work together to achieve the common goal of building the world's finest Free Software. Everyone is welcome to join and contribute to KDE, including you.\n\nVisit https://www.kde.org/ for more information about the KDE community and the software we produce.\n\nThis application is using Qt %1.").arg(QT_VERSION_STR));

	currentDir = QTHOMELOCATION;
	setupDirectoryTree();

	setupDocuments();
	resize(750, 600);

	readSettings();

//	connect(O20_QO("aboutQt"), SIGNAL(released()), this, SLOT(aboutQt()));
	connect(O20_QO("upload"), SIGNAL(released()), this, SLOT(createDocument()));
	connect(O20_QO("browseDisk"), SIGNAL(released()), this, SLOT(createDocument()));
        connect(O20_QO("openCode"),   SIGNAL(released()), this, SLOT(createDocument()));

	connect(O20_QO("openLocation"), SIGNAL(released()), this, SLOT(openLocation()));
        connect(O20_QO("changePosition"), SIGNAL(released()), this, SLOT(statusBarSlot()));
        connect(O20_QO("changePageStyle"), SIGNAL(released()), this, SLOT(statusBarSlot()));

	connect(O20_QO("useDarkMode"), SIGNAL(released()), SLOT(soundSlot()));

	//connect(O20_QO("editor"), SIGNAL(resizeEmit()), this, SLOT(resizeDocument()));
	connect(rootObject(), SIGNAL(pageChanged(int)), this, SLOT(onSwipePageChanged(int)));

	connect(O20_QO("renameDocument"), SIGNAL(accepted()), this, SLOT(renameDocument()));

        connect(O20_QO("fontFamilySelect"), SIGNAL(activated(int)), SLOT(editorInterfaceCommands()));
	connect(O20_QO("fontFamilySelect"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fontSizeSelect"),   SIGNAL(activated(int)), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fontSizeSelect"),   SIGNAL(accepted()), SLOT(editorInterfaceCommands()));

	connect(O20_QO("formatBold"),      SIGNAL(released()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("formatItalic"),    SIGNAL(released()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("formatUnderline"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("formatStrikeout"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("formatOverline"),  SIGNAL(released()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("formatSubscript"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("formatSuperscript"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("colorDialog"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("defaultColor"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("colorFillDialog"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("defaultFill"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("eraseFormat"), SIGNAL(released()), SLOT(editorInterfaceCommands()));

        connect(O20_QO("color00"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color01"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color02"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color03"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color04"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color05"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color06"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color07"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color08"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color09"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color10"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color11"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color12"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color13"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color14"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color15"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color16"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

        connect(O20_QO("fill00"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill01"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill02"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill03"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill04"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill05"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill06"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill07"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill08"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill09"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill10"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill11"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill12"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill13"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill14"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill15"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill16"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

	connect(O20_QO("underline0"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline1"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline2"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline3"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline4"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline5"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("underline6"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

	connect(O20_QO("spacing0"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
	connect(O20_QO("spacing1"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
	connect(O20_QO("spacing2"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
	connect(O20_QO("spacing3"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
	connect(O20_QO("spacing4"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));

        connect(O20_QO("numbering0"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering1"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering2"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering3"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering4"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering5"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering6"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("numbering7"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

        connect(O20_QO("lspacing100"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing115"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing150"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing200"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

        connect(O20_QO("viewFullScreen"), SIGNAL(toggled()), SLOT(onFullScreenRequest()));

	connect(O20_QO("checkSpelling"), SIGNAL(toggled()), SLOT(onCheckSpellingToggled()));

	connect(O20_QO("insertPageBreak"), SIGNAL(released()), SLOT(editorInterfaceParaCommands()));

        connect(O20_QO("alignLeft"),    SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignRight"),   SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignCenter"),  SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignJustify"), SIGNAL(released()), SLOT(editorInterfaceParaCommands()));

        connect(O20_QO("clipboard0"), SIGNAL(released()), m_document, SLOT(cut()));
        connect(O20_QO("clipboard1"), SIGNAL(released()), m_document, SLOT(copy()));
        connect(O20_QO("clipboard2"), SIGNAL(released()), m_document, SLOT(paste()));

	connect(O20_QO("editUndo_"), SIGNAL(clicked()), m_document, SLOT(undo()));
	connect(O20_QO("editUndo"), SIGNAL(triggered()), m_document, SLOT(undo()));
	connect(O20_QO("editRedo"), SIGNAL(triggered()), m_document, SLOT(redo()));

	connect(O20_QO("insertAuthorName"), SIGNAL(released()), SLOT(editorOddCommands()));

	connect(O20_QO("sidebarDrawer"), SIGNAL(closed()), documentItem, SLOT(forceActiveFocus()));

        connect(O20_QO("createNew"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("createNewBlank"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("singleSpaced"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("generalNotes"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("basicBlank"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("businessLetter"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("boldReport"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("studentReport"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("resume"), SIGNAL(released()), SLOT(createNewDocument()));
	connect(O20_QO("resumeCoverLetter"), SIGNAL(released()), SLOT(createNewDocument()));

	connect(O20_QO("trashDocument"), SIGNAL(activated()), SLOT(trashDocument()));
	connect(O20_QO("closeDocument"), SIGNAL(activated()), SLOT(closeDocument()));

	connect(O20_QO("documentSaveAs"),      SIGNAL(released()), SLOT(saveDocumentAs()));
	connect(O20_QO("documentSaveAsPDF"),   SIGNAL(released()), SLOT(saveDocumentAs()));
	connect(O20_QO("documentSaveAsPDF_2"), SIGNAL(released()), SLOT(saveDocumentAs()));

	connect(O20_QO("headingsView"), SIGNAL(itemClicked()), SLOT(jumpToHeadingPos()));

	connect(O20_QO("forceAutosave"), SIGNAL(toggled()), SLOT(setWindowTitle()));

        connect(O20_QO("globalSave"), SIGNAL(released()), SLOT(threadedSave()));
	connect(O20_QO("documentPrint"), SIGNAL(released()), SLOT(printDocument()));

        //connect(O20_QO("formatBold"),      SIGNAL(hovering(int)), SLOT(liveUpdate(int)));

	connect(O20_QO("useDarkMode"),    SIGNAL(toggled()), SLOT(updatePageBackground()));

	connect(O20_QO("internalFileManager"), SIGNAL(itemClicked()), SLOT(setupDirectoryTree()));
	connect(O20_QO("recentFilesList"), SIGNAL(recentFileOpen()), SLOT(openRecentFile()));

	connect(m_document, SIGNAL(cursorPositionChanged()), SLOT(onCursorPositionChanged()));
	connect(m_document, SIGNAL(textChanged()), SLOT(onTextChanged()));

}


    /*Ink::Ink(QString uiDir)
    {
        setWindowIcon(QIcon(":/O20Core/apps/io.gitlab.ko20.ink.svg"));
        
        quickWindow()->setTextRenderType(QQuickWindow::QtTextRendering);
        
        QFont f; f.setFamily("Open Sans"); f.setPointSize(10);
        qApp->setFont(f);
        
        m_dInfo.open = false;
        setSource(QUrl(uiDir.isEmpty() ? "qrc:/QtQuick/O20/io.gitlab.ko20.ink.qml" : QDir(uiDir).filePath("io.gitlab.ko20.ink.qml")));
        setResizeMode(QQuickWidget::SizeRootObjectToView);
        
        setWindowTitle();
        
        rootObject()->setProperty("aboutQtTxt", QString("KDE is a world-wide community of software engineers, artists, writers, translators and creators who are committed to Free Software development. KDE produces the Plasma desktop environment, hundreds of applications, and the many software libraries that support them.\n\nKDE is a cooperative enterprise: no single entity controls its direction or products. Instead, we work together to achieve the common goal of building the world's finest Free Software. Everyone is welcome to join and contribute to KDE, including you.\n\nVisit https://www.kde.org/ for more information about the KDE community and the software we produce.\n\nThis application is using Qt %1.").arg(QT_VERSION_STR));
        
        currentDir = QTHOMELOCATION;
        setupDirectoryTree();
        
        setupDocuments();
        resize(750, 600);

        readSettings();

        connect(O20_QO("exitButton"), SIGNAL(released()), this, SLOT(close()));
        //	connect(O20_QO("aboutQt"), SIGNAL(released()), this, SLOT(aboutQt()));
        connect(O20_QO("upload"), SIGNAL(released()), this, SLOT(createDocument()));
        connect(O20_QO("browseDisk"), SIGNAL(released()), this, SLOT(createDocument()));
        connect(O20_QO("openCode"),   SIGNAL(released()), this, SLOT(createDocument()));
        
        connect(O20_QO("openLocation"), SIGNAL(released()), this, SLOT(openLocation()));
        connect(O20_QO("changePosition"), SIGNAL(released()), this, SLOT(statusBarSlot()));
        connect(O20_QO("changePageStyle"), SIGNAL(released()), this, SLOT(statusBarSlot()));
        
        connect(O20_QO("useDarkMode"), SIGNAL(released()), SLOT(soundSlot()));
        
        //connect(O20_QO("editor"), SIGNAL(resizeEmit()), this, SLOT(resizeDocument()));
        connect(rootObject(), SIGNAL(pageChanged(int)), this, SLOT(onSwipePageChanged(int)));
        
        connect(O20_QO("renameDocument"), SIGNAL(accepted()), this, SLOT(renameDocument()));
        
        connect(O20_QO("fontFamilySelect"), SIGNAL(activated(int)), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fontFamilySelect"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fontSizeSelect"),   SIGNAL(activated(int)), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fontSizeSelect"),   SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
        
        connect(O20_QO("formatSuperscript"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("colorDialog"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("defaultColor"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("colorFillDialog"), SIGNAL(accepted()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("defaultFill"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("eraseFormat"), SIGNAL(released()), SLOT(editorInterfaceCommands()));
        
        connect(O20_QO("color00"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color01"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color02"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color03"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color04"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color05"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color06"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color07"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color08"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color09"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color10"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color11"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color12"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color13"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color14"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color15"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("color16"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        
        connect(O20_QO("fill00"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill01"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill02"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill03"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill04"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill05"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill06"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill07"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill08"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill09"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill10"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill11"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill12"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill13"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill14"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill15"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("fill16"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        
        connect(O20_QO("underline0"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline1"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline2"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline3"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline4"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline5"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("underline6"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

	connect(O20_QO("numbering0"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering1"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering2"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering3"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering4"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering5"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering6"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
	connect(O20_QO("numbering7"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));

        connect(O20_QO("spacing0"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("spacing1"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("spacing2"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("spacing3"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("spacing4"), SIGNAL(triggered()), SLOT(editorInterfaceParaCommands()));
        
        connect(O20_QO("lspacing100"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing115"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing150"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        connect(O20_QO("lspacing200"), SIGNAL(triggered()), SLOT(editorInterfaceCommands()));
        
        connect(O20_QO("viewFullScreen"), SIGNAL(toggled()), SLOT(onFullScreenRequest()));
        
        connect(O20_QO("checkSpelling"), SIGNAL(toggled()), SLOT(onCheckSpellingToggled()));
        
        connect(O20_QO("insertPageBreak"), SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        
        connect(O20_QO("alignLeft"),    SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignRight"),   SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignCenter"),  SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        connect(O20_QO("alignJustify"), SIGNAL(released()), SLOT(editorInterfaceParaCommands()));
        
        connect(O20_QO("clipboard0"), SIGNAL(released()), m_document, SLOT(cut()));
        connect(O20_QO("clipboard1"), SIGNAL(released()), m_document, SLOT(copy()));
        connect(O20_QO("clipboard2"), SIGNAL(released()), m_document, SLOT(paste()));
        
        connect(O20_QO("editUndo_"), SIGNAL(clicked()), m_document, SLOT(undo()));
        connect(O20_QO("editUndo"), SIGNAL(triggered()), m_document, SLOT(undo()));
        connect(O20_QO("editRedo"), SIGNAL(triggered()), m_document, SLOT(redo()));
        
        connect(O20_QO("insertAuthorName"), SIGNAL(released()), SLOT(editorOddCommands()));
        
        connect(O20_QO("sidebarDrawer"), SIGNAL(closed()), documentItem, SLOT(forceActiveFocus()));
        
        connect(O20_QO("createNew"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("createNewBlank"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("singleSpaced"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("generalNotes"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("basicBlank"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("businessLetter"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("boldReport"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("studentReport"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("resume"), SIGNAL(released()), SLOT(createNewDocument()));
        connect(O20_QO("resumeCoverLetter"), SIGNAL(released()), SLOT(createNewDocument()));
        
        connect(O20_QO("trashDocument"), SIGNAL(activated()), SLOT(trashDocument()));
        connect(O20_QO("closeDocument"), SIGNAL(activated()), SLOT(closeDocument()));
        
        connect(O20_QO("documentSaveAs"),      SIGNAL(released()), SLOT(saveDocumentAs()));
        connect(O20_QO("documentSaveAsPDF"),   SIGNAL(released()), SLOT(saveDocumentAs()));
        connect(O20_QO("documentSaveAsPDF_2"), SIGNAL(released()), SLOT(saveDocumentAs()));
        
        connect(O20_QO("headingsView"), SIGNAL(itemClicked()), SLOT(jumpToHeadingPos()));
        
        connect(O20_QO("forceAutosave"), SIGNAL(toggled()), SLOT(setWindowTitle()));
        
        connect(O20_QO("globalSave"), SIGNAL(released()), SLOT(threadedSave()));
        connect(O20_QO("documentPrint"), SIGNAL(released()), SLOT(printDocument()));
        
        //connect(O20_QO("formatBold"),      SIGNAL(hovering(int)), SLOT(liveUpdate(int)));
        
        connect(O20_QO("useDarkMode"),    SIGNAL(toggled()), SLOT(updatePageBackground()));
        
        connect(O20_QO("internalFileManager"), SIGNAL(itemClicked()), SLOT(setupDirectoryTree()));
        connect(O20_QO("recentFilesList"), SIGNAL(recentFileOpen()), SLOT(openRecentFile()));
        
        connect(m_document, SIGNAL(cursorPositionChanged()), SLOT(onCursorPositionChanged()));
        connect(m_document, SIGNAL(textChanged()), SLOT(onTextChanged()));
        
    }*/
    
    // ====================================================================
    
    void Ink::setupDocuments() {
        documentItem = qobject_cast<PreviewItem*>(O20_QO("editor"));
        m_document = documentItem->getDocument();
        m_document->setFrameStyle(0);
        m_document->setUsePageMode(true);
        
        //	documentItem->setStyleName("Windows");
        documentItem->reload();
    }
    
    void Ink::setReadOnly(bool readOnly) { m_dInfo.isReadOnly = true; O20_QOSET("readOnlyButton", "checked", true); }
    
    // ====================================================================
    
    void Ink::jumpToHeadingPos() {
        if (headingPos.isEmpty()) return;
        int a = O20_QOGET("headingsView", "headingIndex").toInt();
        QTextCursor cursor = m_document->textCursor();
        cursor.movePosition(QTextCursor::Start);
        cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, headingPos.at(a));
        m_document->setTextCursor(cursor);
        
        O20_QOSET("sidebarDrawer", "visible", "false");
    }
    
    void Ink::onCursorPositionChanged() {
        if (m_dInfo.textOnly && !m_dInfo.isReadOnly) {
            QTextEdit::ExtraSelection selection;
            selection.format.setBackground(QColor(m_highlighter->theme().editorColor(KSyntaxHighlighting::Theme::CurrentLine)));
            selection.format.setProperty(QTextFormat::FullWidthSelection, true);
            selection.cursor = m_document->textCursor();
            selection.cursor.clearSelection();
            
            QList<QTextEdit::ExtraSelection> extraSelections;
            extraSelections.append(selection);
            m_document->setExtraSelections(extraSelections);
            
            return;
        }
        
        if (m_commandLock) return;
        m_commandLock = true;
        
        QTextCursor cursor = m_document->textCursor();
        QTextCharFormat charFormat = cursor.charFormat();
        
        O20_QOSET("formatBold",   "checked", charFormat.fontWeight() == QFont::Bold);
        O20_QOSET("formatItalic", "checked", charFormat.fontItalic());
        O20_QOSET("formatUnderline", "checked", charFormat.underlineStyle() != QTextCharFormat::NoUnderline);
        O20_QOSET("formatOverline",  "checked", charFormat.fontOverline());
        O20_QOSET("formatStrikeout", "checked", charFormat.fontStrikeOut());
        
        O20_QOSET("formatSuperscript", "checked", charFormat.verticalAlignment() == QTextCharFormat::AlignSuperScript);
        O20_QOSET("formatSubscript",   "checked", charFormat.verticalAlignment() == QTextCharFormat::AlignSubScript);
        
        QString fontFamily = charFormat.fontFamily();
        QString fontSize = QString::number(charFormat.fontPointSize());
        
        QList<QVariant> model1 = O20_QOGET("fontFamilySelect", "model").toList();
        if (fontFamily.isEmpty()) fontFamily = "Open Sans";
        int i = model1.indexOf(fontFamily);
        O20_QOSET("fontFamilySelect", "currentIndex", i);
        if (i == -1) O20_QOSET("fontFamilySelect", "editText", "Unknown");
        
        QList<QVariant> model2 = O20_QOGET("fontSizeSelect", "model").toList();
        if (fontSize == "0") fontSize = "11";
        int i2 = model2.indexOf(fontSize);
        O20_QOSET("fontSizeSelect", "currentIndex", i2);
        if (i2 == -1)  O20_QOSET("fontSizeSelect", "editText", fontSize);
        
        //	O20_QOSET("fontSizeSelect", "editText",   fontSize   == "0" ? QString::number(m_textDocument->defaultFont().pointSize()) : fontSize);
        
        bool useDarkMode = O20_QOGET("useDarkMode", "checked").toBool();
        QString a = charFormat.foreground().color().name();
        O20_QOSET("selectColorButton", "color", useDarkMode && a == "#000000" ? "#ffffff" : a);
        O20_QO("colorDialog")->findChild<QObject*>("line")->setProperty("text", a);
        
        QString b = charFormat.background().color().name();
        O20_QOSET("selectFillButton", "color", useDarkMode && b == "#000000" ? "#ffffff" : b);
        O20_QO("colorFillDialog")->findChild<QObject*>("line")->setProperty("text", b);
        
        QTextBlockFormat blockFormat = cursor.blockFormat();
        if (blockFormat.alignment() == Qt::AlignLeft)
            O20_QOSET("alignLeft", "checked", true);
        else if (blockFormat.alignment() == Qt::AlignRight)
            O20_QOSET("alignRight", "checked", true);
        else if (blockFormat.alignment() == Qt::AlignHCenter or blockFormat.alignment() == Qt::AlignCenter)
            O20_QOSET("alignCenter", "checked", true);
        else if (blockFormat.alignment() == Qt::AlignJustify)
            O20_QOSET("alignJustify", "checked", true);
        
        m_commandLock = false;
    }
    
    void Ink::onTextChanged() {
        onCursorPositionChanged();
        
        
        setWindowTitle();
        if (O20_QOGET("forceAutosave", "checked").toBool())
            threadedSave();
        
        O20_QOSET("editUndo", "enabled", m_textDocument->isUndoAvailable());
        O20_QOSET("editRedo", "enabled", m_textDocument->isRedoAvailable());
        
        //updateTextToStyles();
        
        // START AUTOMATIC HEADING DETECTION
        
        headingPos.clear();
        QStringList headingNames;
        
        bool pageBreakBefore = false;
        int i = 0;
        
        //	bool bigFontSize, 
        
        for (QTextBlock it = m_textDocument->begin(); it != m_textDocument->end(); it = it.next()) {
            int headingLevel = it.blockFormat().headingLevel();
            QString text = it.text();
            
            bool pageBreakNow = it.blockFormat().pageBreakPolicy() == QTextFormat::PageBreak_AlwaysBefore;
            headingLevel = text.contains(tr("Chapter"), Qt::CaseInsensitive) ? 1 : headingLevel;
            
            if (headingLevel == 0) headingLevel = pageBreakBefore || (i == 0 && pageBreakNow) || (it.text() != "" && pageBreakNow)? 1 : headingLevel;
            if (text.isEmpty() && pageBreakNow) pageBreakBefore = true;
            else pageBreakBefore = false;
            
            if (headingLevel > 0 and text != "") headingNames << (QString().fill(' ', headingLevel*2) + text), headingPos << it.position();
        }
        
        if (headingNames.size() == 0) headingNames << "This document has no headings";
        O20_QOSET("headingsView", "model", headingNames);
        
        // END AUTOMATIC HEADING DETECTION
        
        int wordCount = m_document->toPlainText().split(QRegExp("(\\s|\\n|\\r)+"), QString::SkipEmptyParts).count();
        int pageCount = m_document->document()->pageCount();
        if (!m_dInfo.textOnly)
            O20_QOSET("documentMetrics", "text", QString(tr("%1 %2   %3 %4")).arg(pageCount).arg(pageCount == 1 ? "page" : "pages").arg(wordCount)
            .arg(wordCount == 1 ? "word" : "words"));
        else
            O20_QOSET("documentMetrics", "text", QString(tr("%1 %2")).arg(wordCount).arg(wordCount == 1 ? "word" : "words"));
    }
    
    // ====================================================================
    
    void Ink::onFullScreenRequest() {
        setWindowState(O20_QOGET("viewFullScreen", "checked").toBool() ? Qt::WindowFullScreen : Qt::WindowNoState);
    }
    
    // ====================================================================
    
    void Ink::openLocation() {
        if (m_dInfo.path != "")
            QDesktopServices::openUrl(QFileInfo(m_dInfo.path).path());
    }
    
    // ====================================================================
    
    void Ink::renameDocument() {
        QString a = O20_QOGET("renameDocument", "text").toString();

	if (a.isEmpty() or a.isNull())
		return;

        QString b = QFileInfo(m_dInfo.path).dir().filePath(a + (QFileInfo(a).suffix().isEmpty() ? (m_dInfo.suffix.isEmpty() ? "" : "." + m_dInfo.suffix) : ""));
        QFile(m_dInfo.path).rename(b);
        m_dInfo.path = b;
        m_dInfo.name = a;
        
        if (m_dInfo.textOnly) {
            bool useDarkMode = O20_QOGET("useDarkMode", "checked").toBool();
            const auto def = m_repository.definitionForFileName(m_dInfo.path);
            m_highlighter = new KSyntaxHighlighting::SyntaxHighlighter(m_textDocument);
            m_highlighter->setTheme(m_repository.defaultTheme(!useDarkMode ? KSyntaxHighlighting::Repository::LightTheme :
            KSyntaxHighlighting::Repository::DarkTheme));
            m_highlighter->setDefinition(def);
        }
        
        addRecentFile(m_dInfo.path);
        setWindowTitle();

	O20_QOSET("renameDocument", "text", ""); // clear text for next usage
    }
    
    // ====================================================================
    
    void Ink::manageClipboard() {
        if (sender() == O20_QO("clipboard0")) m_document->cut();
        else if (O20_QO("clipboard1")) m_document->copy();
        else if (O20_QO("clipboard2")) m_document->paste();
    }
    
    // ====================================================================
    
    void Ink::editorOddCommands() {
        if (sender() == O20_QO("insertAuthorName")) m_document->insertPlainText(O20_QOGET("fullname", "text").toString());
    }
    
    void Ink::editorInterfaceCommands() {

        if (m_commandLock) return;
        
        QTextCursor cursor = m_document->textCursor();
        
        // font familty has special treatement
        if (sender() == O20_QO("fontFamilySelect")) {
            m_document->setFontFamily(O20_QOGET("fontFamilySelect", "currentText").toString());
            return;
        }
        
        bool hadSelection = cursor.hasSelection(); // automatically select the current word if on the line
        
        //if (!hadSelection) {
        //	cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
        
        //	qDebug() << cursor.selectedText();
        //	if (cursor.selectedText() != " ")
        //		cursor.select(QTextCursor::WordUnderCursor), qDebug("EYYYYYY.");
        //}
        
        QTextCharFormat charFormat = m_document->currentCharFormat(); //cursor.charFormat();
       
        if (sender() == O20_QO("formatBold"))
            charFormat.setFontWeight(charFormat.fontWeight() == QFont::Bold ? QFont::Normal : QFont::Bold);
        else if (sender() == O20_QO("formatItalic"))
            charFormat.setFontItalic(!charFormat.fontItalic());
        //	else if (sender() == O20_QO("formatUnderline"))
        //		charFormat.setFontUnderline(!charFormat.fontUnderline());
        else if (sender() == O20_QO("formatStrikeout"))
            charFormat.setFontStrikeOut(!charFormat.fontStrikeOut());
        else if (sender() == O20_QO("formatOverline"))
            charFormat.setFontOverline(!charFormat.fontOverline());
        else if (sender() == O20_QO("fontSizeSelect"))
            charFormat.setFontPointSize(O20_QOGET("fontSizeSelect", "editText").toInt());
        else if (sender() == O20_QO("eraseFormat"))
            charFormat = QTextCharFormat();
        else if (sender() == O20_QO("formatSuperscript"))
            charFormat.setVerticalAlignment(charFormat.verticalAlignment() == QTextCharFormat::AlignSuperScript ?
            QTextCharFormat::AlignNormal : QTextCharFormat::AlignSuperScript);
        else if (sender() == O20_QO("formatSubscript"))
            charFormat.setVerticalAlignment(charFormat.verticalAlignment() == QTextCharFormat::AlignSubScript ?
            QTextCharFormat::AlignNormal : QTextCharFormat::AlignSubScript);
        else if (sender() == O20_QO("colorDialog")) {
            charFormat.setForeground(QBrush(QColor(O20_QOGET("colorDialog", "color").toString())));
        } else if (sender() == O20_QO("colorFillDialog")) {
            charFormat.setBackground(QBrush(QColor(O20_QOGET("colorFillDialog", "color").toString())));
        } else if (sender() == O20_QO("defaultColor"))
            charFormat.clearForeground();
        else if (sender() == O20_QO("defaultFill"))
            charFormat.clearBackground();
        else if (sender()->objectName().left(8) == "lspacing") {
            qreal spacing = sender()->objectName().right(3).toInt();
            charFormat.setFontLetterSpacing(spacing);
        } else if (sender()->objectName().left(9) == "underline") {
            int index = sender()->objectName().right(1).toInt();
            QTextCharFormat::UnderlineStyle style = (QTextCharFormat::UnderlineStyle) index;
            charFormat.setUnderlineStyle(style);
        } else if (sender()->objectName().left(5) == "color") {
            auto list = O20_QOGET("colorMenu", "colorList").toList();
            int index = sender()->objectName().right(2).toInt();
            charFormat.setForeground(QColor(list[index].toString()));
        } else if (sender()->objectName().left(4) == "fill") {
            auto list = O20_QOGET("colorFillMenu", "colorList").toList();
            int index = sender()->objectName().right(2).toInt();
            charFormat.setBackground(QColor(list[index].toString()));
        }
        
        m_document->setCurrentCharFormat(charFormat);

/*        if (sender()->objectName().left(9) == "numbering" or sender() == O20_QO("listNumberingButton")) {
	    QString type = sender()->property("text").toString();
            QTextListFormat::Style style = QTextListFormat::ListDisc;
                
            if (type == "White Bullet")                                        
                style = QTextListFormat::ListCircle;
            else if (type == "Square Bullet")
                style = QTextListFormat::ListSquare;
            else if (type == "1. 2. 3")
                style = QTextListFormat::ListDecimal;
            else if (type == "A. B. C.")
                style = QTextListFormat::ListUpperAlpha;
            else if (type == "a. b. c")
                style = QTextListFormat::ListLowerAlpha;
            else if (type == "I. II. III")
                style = QTextListFormat::ListUpperRoman;
            else if (type == "i. ii. iii")
                style = QTextListFormat::ListLowerRoman;
                
            QTextListFormat listFormat;
            listFormat.setStyle(style);
	

            QTextList CL = cursor.currentList()'

	    if (CL != nullptr) {
		// remove list
		


	        if (CL->format().style() != style) {
	            	cursor.createList(listFormat);
		}
	    } else {
		cursor.createList(listFormat);
	    }
        }*/

        if (!hadSelection) {
            cursor.clearSelection();
        }
    }
    
    void Ink::editorInterfaceParaCommands() {
        if (m_commandLock) return;
        
        QTextCursor cursor = m_document->textCursor();
        
        // font familty has special treatement
        if (sender() == O20_QO("fontFamilySelect")) {
            m_document->setFontFamily(O20_QOGET("fontFamilySelect", "currentText").toString());
            return;
        }
        
        QTextBlockFormat blockFormat = cursor.blockFormat();
        
        if (sender() == O20_QO("alignLeft"))
            blockFormat.setAlignment(Qt::AlignLeft);
        else if (sender() == O20_QO("alignRight"))
            blockFormat.setAlignment(Qt::AlignRight);
        else if (sender() == O20_QO("alignCenter"))
            blockFormat.setAlignment(Qt::AlignHCenter);
        else if (sender() == O20_QO("alignJustify"))
            blockFormat.setAlignment(Qt::AlignJustify);
        
        if (sender()->objectName().left(7) == "spacing") {
            blockFormat.setLineHeight(sender()->property("text").toReal(), QTextBlockFormat::ProportionalHeight);
        }
        
        if (sender() == O20_QO("insertPageBreak")) {
            m_document->insertPlainText("\n");
	    blockFormat.setPageBreakPolicy(QTextFormat::PageBreak_AlwaysBefore);
        }

        cursor.setBlockFormat(blockFormat);

        if (sender() == O20_QO("insertPageBreak")) {
            m_document->insertPlainText("\n");
            blockFormat = cursor.blockFormat();
            blockFormat.setPageBreakPolicy(QTextFormat::PageBreak_Auto);
            cursor.setBlockFormat(blockFormat);
        }
    }
    
    /* update text when buttons are hovered; postponed for a later release */
    void Ink::liveUpdate(int hovered) {
        /*        QTextCursor cursor = m_document->textCursor();
         *        QTextCharFormat charFormat = cursor.charFormat();
         * 
         *	if (sender() == O20_QO("formatBold")) {
         *		if (hovered == 1) charFormat.setFontWeight(m_liveUpdate.bold ? QFont::Bold : QFont::Normal); // qDebug() << "K!";
         *		else charFormat.setFontWeight(!m_liveUpdate.bold ? QFont::Bold : QFont::Normal), qDebug() << !m_liveUpdate.bold;
    }
    
    cursor.setCharFormat(charFormat); */
    }
    
    // ====================================================================
    
    void Ink::startCore() {
        //resizeDocument();
        ////	if (m_dInfo.name == "") m_document->hide();
        //else m_document->show();
    }
    
    // ====================================================================
    
    void Ink::onSwipePageChanged(int p) {
        if (m_documentVisible) m_document->setHidden(rootObject()->property("currentIndex").toInt() == 0);
        if (p) documentItem->forceActiveFocus();
    }
    
    // ====================================================================
    
    void Ink::onCheckSpellingToggled() {
        QSound::play(":/Sounds/04/ui_refresh-feed.wav");
        
        if (O20_QOGET("checkSpelling", "checked").toBool()) generateSCheckObject();
        else deleteSCheckObject();
    }
    
    void Ink::generateSCheckObject() {
        m_documentDecorator = new Sonnet::SpellCheckDecorator(m_document);
        m_documentDecorator->highlighter()->setMisspelledColor(QColor("#E91E63"));
        O20_QOSET("checkSpelling", "checked", true);
    }
    
    void Ink::deleteSCheckObject() {
        if (m_documentDecorator) {
            delete m_documentDecorator->highlighter();
            delete m_documentDecorator;
        }
        
        O20_QOSET("checkSpelling", "checked", false);
    }
    
    // ====================================================================
    
    void Ink::aboutQt() {
        QMessageBox::aboutQt(this);
    }
    
    // ====================================================================
    
    /* File I/O */
    
    KO20::DocumentInformation Ink::getIoFile(bool openOrSave, QString filters) {
        if (filters.isEmpty() and openOrSave) filters = "Deduct from Extension (*);;Open Document Text (*.odt *.ott *.odf);;HTML Files (*.html *.xhtml *.htm);; Code (*)";
        else if (filters.isEmpty()) filters = "Open Document Text (*.odt *.ott *.odf);;HTML Files (*.html *.xhtml *.htm);;Code (*)";
        
        QFileDialog* qf = new QFileDialog(this);
        qf->selectFile("Document1.odt");
        qf->setAcceptMode(openOrSave ? QFileDialog::AcceptOpen : QFileDialog::AcceptSave);
        qf->setNameFilter(filters);
        
        KO20::DocumentInformation document;
        document.isValid = false;
        
        if (qf->exec() == QDialog::Rejected) {
            /* return an invalid document.
             * the caller of this function MUST checked the m_isValid property */
        } else {
            document.path = qf->selectedFiles()[0];
            document.name   = QFileInfo(document.path).baseName();
            document.suffix = QFileInfo(document.path).suffix();
            
            document.isValid = true;
        }
        
        //	qf->close();
        delete qf;
        return document;
    }
    
    void Ink::threadedSave() {
        if (m_dInfo.name == "") return;
        if (!m_saveLock) m_saveLock = true;
        else return;
        
        QObject* myObject = new QObject;
        QThread* threadt = new QThread;
        threadt->setObjectName("DocumentSaveThread");
        QObject::connect(threadt, &QThread::started, [=]() { officeIo(0); threadt->terminate(); m_saveLock = false; });
        myObject->moveToThread(threadt);
        threadt->start();
    }
    
    #include <functional>
    
    /* mode is Open (true) or Save (false) */
    void Ink::officeIo(int mode, QString path) {
        if (path == "") path = m_dInfo.path;
        QString suffix = QFileInfo(path).suffix();
        
        std::function<void(void)> cleanup = [&]() { m_documentVisible = true; }; //onSwipePageChanged(1); };
        
        //	void (*cleanup)() = [](bool &m, ){ m_documentVisible = true; };
        
        //	void (*cleanup)() = [&]() { m_documentVisible = true; onSwipePageChanged(1); };
        
        m_documentVisible = false;
        
        //rootObject()->setProperty("currentIndex", 1);
        
        if (mode == 1) m_document->hide();
        
        if (mode == 0) {
            //qDebug() << (m_dInfo.isReadOnly and path == m_dInfo.path);
            if (m_dInfo.isReadOnly and path == m_dInfo.path or !m_dInfo.open) {
                cleanup();
                return;
            }
            
            QTextDocumentWriter qtw(path);
            if (suffix == "odt" or suffix == "ott" or suffix == "odf")
                qtw.setFormat("odt");
            else if ((suffix == "html" or suffix == "xhtml") && !m_dInfo.textOnly)
                qtw.setFormat("html");
            else if (suffix == "pdf") {
                printDocument(false, path);
                cleanup();
                return;
            } else
                qtw.setFormat("plaintext");
            
            bool sucess = qtw.write(m_textDocument);
            m_textDocument->setModified(false);
            if (suffix == "odt" or suffix == "ott")
                O20::PatchODT(m_dInfo.path, m_dInfo.styles.stylenames, m_dInfo.styles.charFormats, m_dInfo.styles.blockFormats);
            
        } else if (mode == 1) {
            //m_document = new QTextDocument();
            
            if (suffix == "pdf" or suffix == "doc" or suffix == "png") {
                O20_QOSET("errorDialog", "visible", "true");
                m_dInfo.isValid = false;
                return;
            }
            
            QFont sansFont;
            sansFont.setPointSize(11);
            sansFont.setFamily("Open Sans");
            m_textDocument->setDefaultFont(sansFont);
            
            if (suffix == "odt" or suffix == "ott") {
                O20_QOSET("documentNameToOpen", "text", path);
                O20_QOSET("loadingScreen", "visible", true);
            }
            
            updatePageBackground();
            
            if (suffix == "docx" or suffix == "dotx") {
                QString HTML = O20::MSFormats::convertMSDocument(path);
                m_dInfo.isReadOnly = true;
                m_textDocument->setHtml(HTML);
                generateSCheckObject();
                
                m_dInfo.textOnly = false;
                //path = path + ".odt";
                //m_dInm_dInfo= m_dInfo.name + ".odt";
            } else if (suffix == "odt" or suffix == "ott") {
                
                bool openLock = true;
                OOO::Converter odt;
                QTextDocument *doc;
                
                QObject* myObject = new QObject;
                QThread* threadt = new QThread;
                threadt->setObjectName("DocumentOpenThread");
                QObject::connect(threadt, &QThread::started, [&]() {
                    doc = odt.convert(path);
                    //				m_document->setDocument(doc);
                    //				OOO::StyleInformation* oooStyles = odt.getStyleInformation();
                    //				saveStyles(oooStyles);
                    threadt->terminate(); openLock = false;
                });
                
                myObject->moveToThread(threadt);
                threadt->start();
                
                while (openLock) QCoreApplication::processEvents();
                
                //			OOO::Converter odt;
                //			m_document->setDocument(odt.convert(path));
                
                m_document->setDocument(doc);
                OOO::StyleInformation* oooStyles = odt.getStyleInformation();
                saveStyles(oooStyles);
                m_dInfo.textOnly = false;
                //			generateSCheckObject(); TODO only spellcheck small docs
            } else {
                QFile gtext(path);
                gtext.open(QIODevice::ReadOnly | QIODevice::Text);
                QString documentText = gtext.readAll();
                gtext.close();
                
                m_document->setUsePageMode(false);
                
                if ((suffix == "html" or suffix == "xhtml") && !m_dInfo.textOnly) {
                    m_textDocument->setHtml(documentText);
                    generateSCheckObject();
                } else {
                    bool useDarkMode = O20_QOGET("useDarkMode", "checked").toBool();
                    const auto def = m_repository.definitionForFileName(path);
                    m_highlighter = new KSyntaxHighlighting::SyntaxHighlighter(m_textDocument);
                    m_highlighter->setTheme(m_repository.defaultTheme(!useDarkMode ? KSyntaxHighlighting::Repository::LightTheme :
                    KSyntaxHighlighting::Repository::DarkTheme));
                    m_highlighter->setDefinition(def);
                    
                    m_textDocument->setPlainText("");
                    
                    QFont monoFont;
                    monoFont.setPointSize(11);
                    monoFont.setFamily("Roboto Mono");
                    
                    m_textDocument->setDefaultFont(monoFont);
                    
                    m_dInfo.textOnly = true;
                    m_document->setAcceptRichText(false);
                    m_textDocument->setPlainText(documentText);
                    
                    O20_QOSET("ribbonO", "visible", false);
                    O20_QOSET("ribbonO", "enabled", false);
                }
            }
            
            //m_documenKKK >> t->setDocument(m_document);
            if (!m_dInfo.textOnly) {
                O20_QOSET("changePageStyle", "checked", true);
                m_textDocument->setDefaultFont(sansFont);
            }
            
            m_dInfo.isAutosave = true;
        }
        
        if (m_dInfo.isReadOnly) m_document->setReadOnly(true);
        
        cleanup();
        m_document->show();
    }
    
    void Ink::closeDocument() {
        m_dInfo = DocumentInformation();
        rootObject()->metaObject()->invokeMethod(rootObject(), "restart");
        
        O20_QOSET("ribbonO", "visible", true);
        O20_QOSET("ribbonO", "enabled", true);
        
        QSound::play(":/Sounds/02/alert_high-intensity.wav");
        
        setWindowTitle();
    }
    
    void Ink::trashDocument() {
        
        QSoundEffect* effect = new QSoundEffect();
        effect->setSource(QUrl("qrc:/Sounds/01/hero_decorative-celebration-02.wav"));
        // qrc:/Sounds/01/hero_simple-celebration-03.wav or qrc:/Sounds/01/hero_decorative-celebration-02.wav !?!?
        
        QFile(m_dInfo.path).remove(); // TODO use moveToTrash? where is it going then? >.<
        m_dInfo.path = "";
        m_dInfo.open = false;
        
        hide();
        
        effect->play();
        
        connect(effect, &QSoundEffect::playingChanged, [=]() {
            if (!effect->isPlaying())
                close();
        });
    }
    
    QString generateNewDocPath() {
        QString _filename = "";
        QString _suffix = "odt";
        
        QString docs = qEnvironmentVariableIsSet("SNAP") ? QTHOMELOCATION : QTDOCUMENTSLOCATION;
        
        if(!QDir(docs + "/KO20/Docs").exists()) {
            QDir().mkdir(docs + "/KO20");
            QDir().mkdir(docs + "/KO20/Docs");
        }
        
        int filenameIndex = 0;
        while (true) {
            filenameIndex++;
            _filename = docs + "/KO20/Docs/Document" + QString::number(filenameIndex) + ".odt";
            if (!QFile::exists(_filename)) break;
        }
        
        return _filename;
    }
    
    /* create a document from existing --> new (which can be the same as the existing file) */
    bool Ink::createDocument(QString createFrom, bool createNoExist, QString createTo) {
        if (createTo == "" && !createNoExist) createTo = createFrom;
        
        if (m_dInfo.open) {
            KO20::Ink* n = new KO20::Ink();
            n->setReadOnly(O20_QOGET("readOnlyButton", "checked").toBool());
            n->setTextOnly(sender() == O20_QO("openCode"));
            if (!n->createDocument(createFrom, createNoExist, createTo)) { n->close(); return false; }
            n->startCore();
            n->show();
            
            //rootObject()->setProperty("currentIndex", 1);
            return true;
        }
        
        if (sender() == O20_QO("openCode")) m_dInfo.textOnly = true;
        
        if (createFrom == "" && !createNoExist) {
            KO20::DocumentInformation dInfo = getIoFile(true, m_dInfo.textOnly ? "Code (*.c *.cpp *.cxx *.h *.hpp *.py *.cs *.xml *.svg *.java *.rb *.rbw *.php *.go *.yaml *.toml *.desktop *.rs CMakeLists.txt *.make Makefile* *.cmake);;Text Files (*)" : ""); createTo = createFrom;
            if (!dInfo.isValid) return false;
            if (m_dInfo.textOnly) dInfo.textOnly = true; 
            m_dInfo = dInfo;
        } else {
            
            if (createTo == "")
                createTo = generateNewDocPath(); //getIoFile(true);
            
            m_dInfo.path   = createTo;
            m_dInfo.name   = QFileInfo(m_dInfo.path).baseName();
            m_dInfo.suffix = QFileInfo(m_dInfo.path).suffix();
            
            m_dInfo.isValid = true;
        }
        
        m_dInfo.isReadOnly = O20_QOGET("readOnlyButton", "checked").toBool();
        
        if (createNoExist and !QFileInfo(m_dInfo.path).exists())
            officeIo(1, createFrom), officeIo(0), justCreated = true;
        else
            officeIo(1);
        
        m_dInfo.name = QFileInfo(m_dInfo.path).baseName();
        
        m_dInfo.open = true;
        
        
        if (!m_dInfo.isValid) {
            m_dInfo = DocumentInformation();
            return false;
        }
        
        addRecentFile(m_dInfo.path);
        
        documentItem->update();
        
        if (!QFileInfo(m_dInfo.path).isReadable() and !justCreated)
            O20_QOSET("criticalErrorButton", "visible", true), m_dInfo.open = false, QSound::play(":/Sounds/04/alert_error-01.wav");
        else if (!QFileInfo(m_dInfo.path).isWritable() and !createNoExist) // XXX XXX XXX badd
            O20_QOSET("warningButton", "visible", true), m_dInfo.isReadOnly = true, QSound::play(":/Sounds/04/alert_error-02.wav");
        else QSound::play(":/Sounds/02/notification_simple-02.wav");
        
        if (createNoExist) officeIo(0);
        
        rootObject()->setProperty("documentEdit", true);
        rootObject()->setProperty("currentIndex", 1);
        O20_QOSET("loadingScreen", "visible", false);
        setWindowTitle();
        updatePageBackground();
        m_document->moveCursor(QTextCursor::Start);
        documentItem->forceActiveFocus();
        onCursorPositionChanged();
        return true;
    }
    
    void Ink::saveDocumentAs() {
        KO20::DocumentInformation dInfo = getIoFile(false, (sender() == O20_QO("documentSaveAsPDF") or sender() == O20_QO("documentSaveAsPDF_2")) ? "PDF Files (*.pdf)" : "");
        if (dInfo.name == "") return;
        
        officeIo(0, dInfo.path);
    }
    
    void Ink::openRecentFile() {
        qreal a = O20_QOGET("recentFilesList", "listIndex").toReal();
        QString fname = recentFilesList.at(a);
        
        createDocument(fname, false);
    }
    
    void Ink::createNewDocument() {
        QString templateClass;
        
        if (sender() == O20_QO("singleSpaced")) templateClass = "SingleSpacedBlank.odt";
        else if (sender() == O20_QO("generalNotes")) templateClass = "GeneralNotes.odt";
        else if (sender() == O20_QO("basicBlank")) templateClass = "BasicDesignBlankTemplate.odt";
        else if (sender() == O20_QO("businessLetter")) templateClass = "FormalBuisnessLetter.odt";
        else if (sender() == O20_QO("boldReport")) templateClass = "BoldReport.odt";
        else if (sender() == O20_QO("studentReport")) templateClass = "SimpleStudentReport.odt";
        else if (sender() == O20_QO("resume")) templateClass = "ResumeGreen.odt";
        else if (sender() == O20_QO("resumeCoverLetter")) templateClass = "ResumeCoverLetterGreen.odt";
        else templateClass = "SingleSpacedBlank.odt";
        
        
        if (templateClass != "") templateClass = ":/Templates/files/"+templateClass;
        //templateClass = "/home/miles/Documents/The Evil Deeds of Starburst.odt";
        
        justCreated = true;
        createDocument(templateClass, true);
    }
    
    void Ink::printDocument(bool useDialog, QString document) {
        bool wasOn = O20_QOGET("checkSpelling", "checked").toBool();
        deleteSCheckObject(); // THIS ENSURES THAT SPELL CHECKING DOES NOT APPEAR IN THE PDF!
        
        QPrinter printer(QPrinter::PrinterResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        if (document != "") printer.setOutputFileName(document);
        printer.setPageSize(QPrinter::Letter);
        printer.setPageMargins(.1, .1, .1, .1, QPrinter::Inch);
        
        QPrintDialog printDialog(&printer);
        
        bool accepted;
        if (useDialog) accepted = printDialog.exec() == QDialog::Accepted;
        
        if (accepted or !useDialog) {
            QSizeF pageSizeBefore = m_textDocument->pageSize();
            m_textDocument->setPageSize(printer.pageRect().size());
            m_textDocument->print(&printer);
            m_textDocument->setPageSize(pageSizeBefore);
        }
        
        if (wasOn) generateSCheckObject(); // remake spell checker
    }
    
    /*void Ink::saveDocument() {
     *	
}*/
    
    // ====================================================================
    
    void Ink::setupDirectoryTree() {
        
        QString filePath = QDir(currentDir).filePath(O20_QOGET("internalFileManager", "currentItem").toString());
        if (QFileInfo(filePath).isDir()) currentDir = filePath;
        else {
            createDocument(filePath, false);
        }
        
        QDir d(currentDir);
        
        if (!d.exists() or !d.isReadable()) return;
        d.setFilter(QDir::Files | QDir::Dirs | QDir::NoDot);
        d.setSorting(QDir::Name | QDir::DirsFirst);
        
        QList<QFileInfo> dirList = d.entryInfoList();
        QStringList dirList2;
        QStringList dirList3;
        
        int i = 0;
        for (auto& xxx : dirList) {
            dirList2 << dirList.at(i).filePath();
            dirList3 << dirList.at(i).fileName();
            i++;
        }
        
        
        O20_QOSET("internalFileManager", "iconModel", getMimetypes(dirList2));
        O20_QOSET("internalFileManager", "model", dirList3);
        
    }
    
    // ====================================================================
    
    void Ink::addRecentFile(QString filename) {
        if (recentFilesList.contains(filename))
            recentFilesList.removeAt(recentFilesList.indexOf(filename));
        
        recentFilesList.prepend(filename);
        
        if (recentFilesList.length() > 35) recentFilesList.removeLast();
        
        updateRecentFiles();
    }
    
    void Ink::updateRecentFiles() {
        QStringList fancyInfo;
        
        for (QString file : recentFilesList) {
            // TODO a bit of an ugly hack here so newly created documents aren't removed
            if (!QFileInfo(file).exists() and (m_dInfo.path != file and !justCreated)) { 
                recentFilesList.removeAt(recentFilesList.indexOf(file));
            }
        }
        
        for (QString file : recentFilesList) {
            QFileInfo f(file);
            QString name = f.baseName();
            fancyInfo << name + (name.size() >= 12 ? "\t" : "\t\t") + f.suffix() + "\t" + f.lastModified().toString("ddd MMMM d yy hh:mm:ss");
        }
        
        O20_QOSET("recentFilesList", "model", fancyInfo);
        O20_QOSET("recentFilesList", "iconModel", getMimetypes(recentFilesList));
        
        QSettings settings;
        settings.beginGroup("RecentStuff");
        settings.setValue("recentFiles", recentFilesList);
        settings.endGroup();
    }
    
    QString Ink::icon_for_filename(const QString &filename) {
        
        QList<QMimeType> mime_types = mime_database.mimeTypesForFileName(filename);
        
        for (auto& mimetype : mime_types)
            return mimetype.iconName();
        
        return "text/plain";
    }
    
    QStringList Ink::getMimetypes(QStringList list) {
        QStringList mimetypes;
        
        for (QString file : list) {
            QString sfx = "text-x-plain";
            
            if (!QFileInfo(file).isDir())
                sfx = icon_for_filename(file).replace("/", "-x-");
            else {
                QString name = QDir(file).dirName();
                
                if (name == "Desktop")
                    sfx = "desktop";
                else if (name == "Documents")
                    sfx = "qrc:/Icons/folder-documents.svg"; // TODO make patch to breeze-icons without the bad headers
                    else if (name == "Pictures")
                        sfx = "folder-pictures";
                    else if (name == "Vidoes")
                        sfx = "folder-videos";
                    else if (name == "Public")
                        sfx = "folder-public";
                    else if (name == "Templates")
                        sfx = "folder-templates";
                    else if (name == "Music")
                        sfx = "folder-music";
                    else
                        sfx = "folder";
            }
            
            mimetypes << sfx;
        }
        
        return mimetypes;
    }
    
    // ====================================================================
    
    void Ink::writeSettings() {
        QSettings settings;
        settings.beginGroup("UserSettings");
        settings.setValue("myNickname", O20_QOGET("nickname", "text"));
        settings.setValue("myFullname", O20_QOGET("fullname", "text"));
        settings.setValue("myInitials", O20_QOGET("initials", "text"));
        
        settings.setValue("useDarkMode", O20_QOGET("useDarkMode", "checked"));
        settings.endGroup();
        
        settings.beginGroup("WindowSettings");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.endGroup();
    }
    
    void Ink::readSettings() {
        QSettings settings;
        settings.beginGroup("UserSettings");
        O20_QOSET("nickname", "text", settings.value("myNickname").toString());
        O20_QOSET("fullname", "text", settings.value("myFullname").toString());
        O20_QOSET("initials", "text", settings.value("myInitials").toString());
       
        O20_QOSET("useDarkMode", "checked", settings.value("useDarkMode").toBool());
        settings.endGroup();
        
        settings.beginGroup("RecentStuff");
        recentFilesList = settings.value("recentFiles").value<QStringList>();
        settings.endGroup();
        
        updateRecentFiles();
        O20_QO("useDarkMode")->metaObject()->invokeMethod(O20_QO("useDarkMode"), "toggled");
        
        //    updatePageBackground();
        
        settings.beginGroup("WindowSettings");
        resize(settings.value("size", QSize(750,600)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        settings.endGroup();
    }
    
    // ====================================================================
    
    void Ink::statusBarSlot() {
        if (sender() == O20_QO("changePosition")) {
            QSound::play(":/Sounds/04/ui_refresh-feed.wav");
            bool checked = O20_QOGET("changePosition", "checked").toBool();
            m_document->moveCursor(checked ? QTextCursor::End : QTextCursor::Start);
        } else if (sender() == O20_QO("changePageStyle")) {
            QSound::play(":/Sounds/04/ui_refresh-feed.wav");
            bool checked = O20_QOGET("changePageStyle", "checked").toBool();
            documentItem->setUsePageMode(checked);
            updatePageBackground();
        }
    }
    
    void Ink::soundSlot() {
        if (sender() == O20_QO("useDarkMode")) {
            QSound::play(":/Sounds/03/ui_tap-variant-01.wav");
        }
    }
    
    // ====================================================================
    
    void Ink::updatePageBackground() {
        //if (!m_dInfo.textOnly) return;
        
        bool useDarkMode = O20_QOGET("useDarkMode", "checked").toBool();
        //bool useDarkPage = O20_QOGET("darkTextSwitch", "checked").toBool();
        
        if (useDarkMode) {
            m_document->setBackgroundColor("#303030", "#ffffff");
            m_document->setPageColor("#101010");
            m_document->setPageBorderColor("#202020");
        } else {
            m_document->setBackgroundColor("#f0f0f0", "#000000");
            m_document->setPageColor("#ffffff");
            m_document->setPageBorderColor("#c6c6c6");
        }
        
        if (m_dInfo.textOnly) {
            const auto def = m_repository.definitionForFileName(m_dInfo.path);
            m_highlighter = new KSyntaxHighlighting::SyntaxHighlighter(m_textDocument);
            m_highlighter->setTheme(m_repository.defaultTheme(!useDarkMode ? KSyntaxHighlighting::Repository::LightTheme :
            KSyntaxHighlighting::Repository::DarkTheme));
            m_highlighter->setDefinition(def);
            
            onCursorPositionChanged();
        }
    }
    
    // ====================================================================
    
    void Ink::saveStyles(OOO::StyleInformation* o) {
        if (m_dInfo.styles.stylenames.size() == 0) {
            m_dInfo.styles.stylenames << "Normal" << "Title" << "Subtitle" << 
            "H1" << "H2" << "H3" << "H4" << "H5" << "H6" << "H7" << "H8" << "H9" <<
            "Emphasis" <<  "Strong" << "Quote" << "Reference" << "Book Title";
            
            QStringList visibleStyles = QStringList() << "Normal" << "Title" << "Subtitle" <<
            "Heading 1" << "Heading 2" << "Heading 3" <<
            "Heading 4" << "Heading 5" << "Heading 6" <<
            "Heading 7" << "Heading 8" << "Heading 9" <<
            "Emphasis" << "Strong" << "Quote" << "Reference" <<
            "Book Title";
            
            O20_QOSET("styleMenu", "stylev", visibleStyles);
            O20_QOSET("styleMenu", "styles", m_dInfo.styles.stylenames);
            
        }
        
        for (QString stylename : m_dInfo.styles.stylenames) {
            QTextCharFormat fmt;
            QTextBlockFormat bfmt = m_document->textCursor().blockFormat();
            
            /*TODO bfmt.setHeadingLevel(...); then have a sidebar with heading navigation */
            
            if (stylename == "Normal") ; // do nothing
            else if (stylename == "H1") {
                fmt.setForeground(QBrush("#2f5496"));
                fmt.setFontPointSize(16);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(1);
            } else if (stylename == "H2") {
                fmt.setForeground(QBrush("#5367a1"));
                fmt.setFontPointSize(13);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(2);
            } else if (stylename == "H3") {
                fmt.setForeground(QBrush("#647492"));
                fmt.setFontPointSize(12);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(3);
            } else if (stylename == "H4") {
                fmt.setForeground(QBrush("#758eb7"));
                fmt.setFontPointSize(11);
                fmt.setFontItalic(true);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(4);
            } else if (stylename == "H5") {
                fmt.setForeground(QBrush("#5976a9"));
                fmt.setFontPointSize(11);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(5);
            } else if (stylename == "H6") {
                fmt.setForeground(QBrush("#8c98ae"));
                fmt.setFontPointSize(11);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(6);
            } else if (stylename == "H7") {
                fmt.setForeground(QBrush("#808da5"));
                fmt.setFontPointSize(11);
                fmt.setFontItalic(true);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(7);
            } else if (stylename == "H8") {
                fmt.setForeground(QBrush("#2b579a"));
                fmt.setFontPointSize(10.5);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(8);
            } else if (stylename == "H9") {
                fmt.setForeground(QBrush("#2b579a"));
                fmt.setFontPointSize(10.5);
                fmt.setFontItalic(true);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(9);
            } else if (stylename == "Title") {
                fmt.setFontPointSize(28);
                fmt.setFontFamily("Open Sans Light");
                bfmt.setHeadingLevel(1);
            } else if (stylename == "Subtitle") {
                fmt.setForeground(QBrush("#7c7d7d"));
                fmt.setFontPointSize(11);
                fmt.setFontFamily("Open Sans Light");
                //bfmt.setHeadingLevel(1);
            } else if (stylename == "Emphasis") {
                fmt.setFontItalic(true);
            } else if (stylename == "Strong") {
                fmt.setFontWeight(QFont::Bold);
            } else if (stylename == "Quote") {
                fmt.setFontItalic(true);
                bfmt.setAlignment(Qt::AlignHCenter);
                bfmt.setTopMargin(10);
                bfmt.setBottomMargin(8);
                bfmt.setLineHeight(108, QTextBlockFormat::ProportionalHeight);
            } else if (stylename == "Reference") {
                fmt.setFontCapitalization(QFont::AllUppercase);
                fmt.setForeground(QBrush("#5a5a5a"));
            } else if (stylename == "Book Title") {
                fmt.setFontItalic(true);
                fmt.setFontWeight(QFont::Bold);
            }
            
            if (o != nullptr) {
                QString s2 = stylename;
                QString ooo = s2.remove(" ");
                if (o->containsStyleFormatProperty(ooo)) {
                    auto textFormat = o->styleProperty(ooo).textFormat();
                    textFormat.apply(&fmt);
                    auto paraFormat = o->styleProperty(ooo).paraFormat();
                    paraFormat.apply(&bfmt);
                }
            }
            
            //QObject *child = O20_QO(stylename);
            QQuickItem *child  = O20::findChild<QQuickItem*>(rootObject()->findChild<QQuickItem*>("styleRepeater"), stylename);
            if (child) {
                child->setProperty("fontFamily", fmt.fontFamily());
                child->setProperty("fontPointSize", fmt.fontPointSize());
            }
            
            QQuickItem* subChild = O20::findChild<QQuickItem*>(rootObject()->findChild<QQuickItem*>("styleRepeater"), stylename+"-refresh");
            
            connect(child, SIGNAL(triggered()), SLOT(connectStyleSlots()));
            connect(subChild, SIGNAL(clicked()), SLOT(updateStyles()));
            
            m_dInfo.styles.charFormats[stylename] = fmt;
            m_dInfo.styles.blockFormats[stylename] = bfmt;
        }
        
        updateTextToStyles();
    }
    
    void Ink::updateStyles() {
        
        bool autodetect = true; // TODO implement office365 style of styles updating
        
        QString currentstylename = sender()->parent()->objectName();
        
        if (true) {
            QTextCharFormat currentFormat = m_dInfo.styles.charFormats[currentstylename];
            if (!autodetect) {
                //            bool ok;
                //            QFont font = QFontDialog::getFont(&ok, m_dInfo.styles.charFormats[currentstylename].font(), this);
                //	    if (ok) m_dInfo.styles.charFormats[currentstylename].setFont(font);
                //	    else return;
            } else {
                m_dInfo.styles.charFormats[currentstylename]  = m_document->textCursor().charFormat();
                m_dInfo.styles.blockFormats[currentstylename] = m_document->textCursor().blockFormat();
                if (currentstylename.contains("Heading"))
                    m_dInfo.styles.blockFormats[currentstylename].setHeadingLevel(currentstylename.at(currentstylename.size()-1).digitValue()); // get the heading value
                    else if (currentstylename.contains("Title"))
                        m_dInfo.styles.blockFormats[currentstylename].setHeadingLevel(1);
            }
            
            QTextCursor c = m_document->textCursor();
            int pos = c.position();
            c.clearSelection();
            for (QTextBlock it = m_textDocument->begin(); it != m_textDocument->end(); it = it.next()) {
                c.setPosition(it.position());
                if (c.charFormat().font() == currentFormat.font()) {
                    c.setPosition(it.position());
                    c.select(QTextCursor::BlockUnderCursor);
                    c.setCharFormat(m_dInfo.styles.charFormats[currentstylename]);
                    c.clearSelection();
                    c.setBlockFormat(m_dInfo.styles.blockFormats[currentstylename]);
                }
            }
            
            c.clearSelection();
            c.setPosition(pos);
        }
        
        sender()->parent()->setProperty("fontFamily", m_dInfo.styles.charFormats[currentstylename].fontFamily());
        sender()->parent()->setProperty("fontPointSize", m_dInfo.styles.charFormats[currentstylename].fontPointSize());
    }
    
    void Ink::updateTextToStyles() {
        QTextCursor c = m_document->textCursor();
        int originalPos = c.position();
        
        for (QTextBlock it = m_textDocument->begin(); it != m_textDocument->end(); it = it.next()) {
            c.setPosition(it.position());
            QFont f = it.charFormat().font();
            
            for (int i = 1; i < m_dInfo.styles.stylenames.size(); ++i) {
                QString stylename = m_dInfo.styles.stylenames[i];
                QFont f2 = m_dInfo.styles.charFormats[stylename].font();
                if (f == f2) {
                    auto a = it.blockFormat();
                    a.setHeadingLevel(m_dInfo.styles.blockFormats[stylename].headingLevel());
                    c.select(QTextCursor::BlockUnderCursor);
                    a.setHeadingLevel(m_dInfo.styles.blockFormats[stylename].headingLevel());
                    c.select(QTextCursor::BlockUnderCursor);
                    a.setHeadingLevel(m_dInfo.styles.blockFormats[stylename].headingLevel());
                    c.select(QTextCursor::BlockUnderCursor);
                    c.mergeBlockFormat(a);
                    break;
                }
            }
        }
        
        c.setPosition(originalPos);
    }
    
    void Ink::connectStyleSlots()
    {
        QString stylename = sender()->objectName();
        
        QTextCursor c = m_document->textCursor();
        
        bool hadSelection = c.hasSelection();
        
        if (!hadSelection) c.clearSelection();
        if (!hadSelection) c.select(QTextCursor::BlockUnderCursor);
        if (m_dInfo.styles.charFormats[stylename].foreground() == QBrush("#000000"))
            m_dInfo.styles.charFormats[stylename].clearForeground();
        c.setCharFormat(m_dInfo.styles.charFormats[stylename]);
        if (!hadSelection) c.clearSelection();
        c.setBlockFormat(m_dInfo.styles.blockFormats[stylename]);
        m_document->setTextCursor(c);
        
        updateTextToStyles();
        //   onCursorPositionChanged(); // update buttons
    }
    
    
    
    // ====================================================================
    
    /* This overrides the default setWindowTitle() function by
     * generating a title based on the document name and other parameters
     * IF the given input value 't' is empty. */
    
    void Ink::setWindowTitle(QString t) {
        m_dInfo.isAutosave = O20_QOGET("forceAutosave", "checked").toBool();
        
        if (t == "") {
            if (m_dInfo.name == "")
                t = "KO20.Ink";
            else if (!m_dInfo.open)
                t = m_dInfo.path + " (Read Error)";
            else if (m_dInfo.isReadOnly)
                t = m_dInfo.path + " (ReadOnly)";
            else if (!m_dInfo.isAutosave)
                if (m_textDocument->isModified())
                    t = m_dInfo.path + " (Edited)";
                else
                    t = m_dInfo.path + " (Saved)";
                else
                    t = m_dInfo.path + " (AutoSave)";
        }
        
        QQuickWidget::setWindowTitle(t);
    }
    
    void Ink::closeEvent(QCloseEvent* event) {
        //threadedSave();
        while (m_saveLock);
        officeIo(0);
        writeSettings();
        event->accept();
    }
    
};
