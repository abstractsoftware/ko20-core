import QtQuick 2.12
import QtQuick.Layouts  1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Pane {
    Material.background: Material.theme == Material.Light ? "white" : "#101010";
    Material.elevation: 2

    Layout.fillWidth: true

    implicitHeight: 92

//    topPadding: 92-50
    property var txt: "O20"
//    property var family: "Open Sans"
    property var size: 22
    property var showLine: true

    ColumnLayout {
    anchors.fill: parent
    Item { Layout.fillHeight: true; }

    RowLayout {
        property var txt: "O20"

        spacing: 10

        Rectangle {
            visible: showLine
            color: themeColor
	    height: 50
            width: 2
        }

        Label {
            text: txt
            font.family: family
            font.pointSize: size
            verticalAlignment: Qt.AlignVCenter
            height: 50
        }

        Item { Layout.fillWidth: true; }
    }
    }
}
