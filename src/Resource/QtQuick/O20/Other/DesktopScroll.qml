import QtQuick 2.12
import QtQuick.Controls 2.12

ScrollView {
	id: desktopScroll

                        ScrollBar.horizontal.interactive: false
                        ScrollBar.vertical.interactive: false

                        clip: true
}
