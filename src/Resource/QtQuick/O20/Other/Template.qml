import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

Button {
    implicitWidth: 152+100
    implicitHeight: 198+100

//    font.family: "Open Sans"
//    font.pointSize: 10

    property var txt: "e" 
    property var icn: "../../../thumbnail-2.svg"

    padding: 50

    icon.color: "transparent"

    ColumnLayout {
        anchors.fill: parent

        Item { Layout.fillHeight: true }

        Rectangle {
            id: rect
            Layout.alignment: Qt.AlignHCenter; width: 152; height: 198; border.color: "#c6c6c6"; color: "white"
            Image {
		anchors.fill: parent
                //anchors.margins: 5

               source: icn; //"../../../Templates/pic/LT02786999.png"
           }
        }

        Item { height: 10; }

        Label {
            Layout.alignment: Qt.AlignHCenter;
            Layout.fillWidth: true
            height: 30

            text: txt
            horizontalAlignment: Qt.AlignHCenter
        }

        Item { Layout.fillHeight: true }
    }

    display: AbstractButton.TextUnderIcon
}
