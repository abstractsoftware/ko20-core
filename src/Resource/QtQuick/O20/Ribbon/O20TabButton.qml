import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

TabButton {
	id: control

	implicitWidth: 62;
	implicitHeight: 30;

	topPadding: 5
	bottomPadding: 5
	leftPadding: 5
	rightPadding: 5

	contentItem: Text {
		font.capitalization: Font.MixedCase 
		font.family: "Open Sans"
		font.pointSize: 10

                color: Material.theme == "Light" ? black : white
		horizontalAlignment: Text.AlignHCenter
		text: control.text
	}
}
