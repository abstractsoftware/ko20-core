import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtQuick.Controls.Material 2.12

import "Buttons" as O20BC
import "Other"   as O20OC

import "o20Scripting.js" as JS

//O20OC.DesktopScroll {
Pane {
    id: sidebar

    Material.accent: Qt.darker(themeColor, 0.5)
    Material.theme: Material.Dark

    padding: 0

    property var sW: !documentEdit ? 120 : 120/2
    property var smallButtonWidth: sW

    property var buttonsRef: [homeButton, newButton, openButton, saveButton, aboutButton, optionsButton]

    height: 500;

    Material.elevation: 3
    Material.background: themeColor

/*      SequentialAnimation on Material.background {
          loops: Animation.Infinite
          ColorAnimation { to: Qt.darker(themeColor, 1.5); duration: 5000; }
          ColorAnimation { to: themeColor; duration: 5000; }
      } */

    Rectangle {
        width: 3
        height: sidebarButton.height

        x: sidebarButton.width - 3 

        color: sidebarButton.checked ? Qt.lighter(themeColor, /*1.3*/2.6) : "transparent"
     }


     ColumnLayout {
         id: sidebarColumnLayout
         anchors.fill: parent
         //height: sidebar.height

         spacing: 0

         Item { height: 92/2; visible: documentEdit; }

         O20BC.SidebarButton {
             id: goBack

            small: true
            checkable: false
            icon.name: "go-previous"

            visible: documentEdit
            onClicked: JS.editDocument(), ribbonTabs.currentIndex = 1;
         }
        
         Rectangle {
              visible: !documentEdit
              height: 92
              Layout.fillWidth: true
              color: "transparent"

              Text {
                  anchors.fill: parent
                  font.pointSize: 14
                  font.family: "Ubuntu"

                  color: "white"
                  text: appName

                  horizontalAlignment: Text.AlignHCenter
                  verticalAlignment: Text.AlignVCenter
                  topPadding: 40
              }
         }


         O20BC.SidebarButton {
             id: homeButton

             small: documentEdit
             checked: true
             icon.name: "go-home"
         }

         O20BC.SidebarButton {
             id: newButton

             small: documentEdit
             icon.name: "document-new-from-template"
         }

         O20BC.SidebarButton {
             id: openButton
             small: documentEdit
             icon.name: "document-open"
         }

         O20BC.SidebarButton {
             id: saveButton
             visible: documentEdit
             small: documentEdit
             icon.name: "document-save"
         }

         Item { Layout.fillHeight: true }


         O20BC.SidebarButton {
             id: aboutButton
             small: documentEdit
             implicitHeight: documentEdit ? sidebar.sW: sidebar.sW/2
             icon.name: "help-whatsthis"
         }

         O20BC.SidebarButton {
             id: optionsButton
             small: documentEdit
             implicitHeight: documentEdit ? sidebar.sW: sidebar.sW/2
             icon.name: "configure"
         }
    }
}
//}
