import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

import "Buttons" as O20BC
import "Dialogs" as O20DG
import "Other"   as O20OC

import "o20Scripting.js" as JS

//ScrollView {
//    clip: true

ColumnLayout {
    id: columnLayout
    
    spacing: 0
    function hideRibbon() {   ribbonO.visible = false, ribbonTabs.visible = false }
    function showTabsOnly() { ribbonO.visible = false, ribbonTabs.visible = true }
    function showTabsAndCommands() { ribbonO.visible = true, ribbonTabs.visible = true }
    
    //anchors.fill: parent
    
    Material.accent: Material.theme == Material.Light ? themeColor : "#ffffff";
    
    Pane {
        id: ribbon
        padding: 0
        
        Layout.fillWidth: true
        Material.accent: Qt.lighter(themeColor, 1.3)
        
        height: ribbonO.visible ? 48*2+8 : 48            
        Material.elevation: ribbonO.visible ? 1 : 0
        
        
        ColumnLayout {
            spacing: 0
            anchors.fill: parent
            
            Pane {
                Layout.fillWidth: true
                implicitHeight: 42
                
                Layout.margins: 5
                
                padding: 0
                Material.elevation: 2
                Material.background: themeColor;
                //background: Rectangle { color: "#31363b"; }
                
                Material.theme: Material.Dark
                Material.accent: "white"
                
                TabBar {
                    id: ribbonTabs
                    
                    anchors.centerIn: parent
                    currentIndex: 1
                    leftPadding: 20
                    
                    O20BC.TabButton { text: qsTr("File"); width: 62; implicitHeight: 42; onClicked: { currentIndex = 0; ribbonTabs.currentIndex = 1; } }
                    O20BC.TabButton { text: qsTr("Home"); width: 62; implicitHeight: 42; onDoubleClicked: { ribbonO.visible?showTabsOnly():showTabsAndCommands(); } }
                    O20BC.TabButton { text: qsTr("Insert"); width: 62; implicitHeight: 42; onDoubleClicked: { ribbonO.visible?showTabsOnly():showTabsAndCommands(); } }
//                    O20BC.TabButton { text: qsTr("Review"); width: 62; onDoubleClicked: { ribbonO.visible?showTabsOnly():showTabsAndCommands(); } }
//                    O20BC.TabButton { text: qsTr("Help"); width: 62; onDoubleClicked: { ribbonO.visible?showTabsOnly():showTabsAndCommands(); } }
                    
                }
                
                RowLayout {                        
                    spacing: 0
                    
                    anchors.fill: parent
                    
                    Item { width: 20 }
                    
                    Label { text: qsTr("AutoSave") }
                    Switch { id: autosave; objectName: "forceAutosave"; focusPolicy: Qt.NoFocus; Layout.fillHeight: true; checked: true; }
                    ToolButton {
                        id: save;
                        objectName: "globalSave";
                        focusPolicy: Qt.NoFocus
                        visible: !autosave.checked
                        icon.name: "document-save"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42
                    }

                    ToolButton {
                        objectName: "documentSaveAsPDF_2";
                        focusPolicy: Qt.NoFocus
                        icon.source: "../../Icons/pdflatex.svg"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42
                    }

                    ToolButton {
			objectName: "editUndo_"
                        focusPolicy: Qt.NoFocus
                        icon.name: "edit-undo"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42

                        onPressAndHold: {
                            undoRedoMenu.width = 56
                            undoRedoMenu.popup()
                        }

                        Menu {
                            id: undoRedoMenu
                            MenuItem { objectName: "editUndo"; icon.name: "edit-undo" }
                            MenuItem { objectName: "editRedo"; icon.name: "edit-redo" }
                        }
                    }
                    
                    Item { Layout.fillWidth: true }

                    ToolButton {
                        focusPolicy: Qt.NoFocus
                        icon.name: "view-right-pane-symbolic"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42
                        onClicked: sidebar.visible = true
                    }
 
                    Label { text: (myNickname == "" ? qsTr("Anonymous") : myNickname) + (myInitials == "" ? "" : ("  ("+myInitials+")")); padding: 10; }
                    
                    
                    Item { width: 5 }
                }
            }
            
            StackLayout {
                objectName: "ribbonO"
                id: ribbonO
                
                Layout.fillWidth: true
                Layout.fillHeight: true
                
                currentIndex: ribbonTabs.currentIndex
                
                Item { }
                
                ScrollView {
                    clip: true
                    ScrollBar.horizontal.policy: ScrollBar.AsNeeded
                    ScrollBar.horizontal.interactive: true
                    
                    RowLayout {
                        spacing: 2
                        Layout.topMargin: 15
                        
                        //columns: 25; //(Math.ceil(1/(ribbonItem.width/255)))*25
                        
                        Menu {
                            id: contextMenu
                            MenuItem { text: qsTr("Cut"); objectName: "clipboard0"; icon.name: "edit-cut"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }
                            MenuItem { text: qsTr("Copy"); objectName: "clipboard1"; icon.source: "../../Icons/edit-copy.svg"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }
                            MenuItem { text: qsTr("Paste"); objectName: "clipboard2";icon.name: "edit-paste"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }
                        }
                        
                        Menu {
                            objectName: "colorMenu"
                            id: colorMenu
                           
                            property var colorList: ["#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#4CAF50", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548"]
 
                            MenuItem { text: qsTr("Default"); objectName: "defaultColor"; icon.name: "edit-clear-all"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }

                            MenuItem { text: qsTr("Red"); objectName: "color00"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[0]; }
                            MenuItem { text: qsTr("Pink"); objectName: "color01"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[1]; }
                            MenuItem { text: qsTr("Purple"); objectName: "color02"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[2]; }
                            MenuItem { text: qsTr("Deep Purple"); objectName: "color03"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[3]; }
                            MenuItem { text: qsTr("Indigo"); objectName: "color04"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[4]; }
                            MenuItem { text: qsTr("Blue"); objectName: "color05"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[5]; }
                            MenuItem { text: qsTr("Light Blue"); objectName: "color06"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[6]; }
                            MenuItem { text: qsTr("Cyan"); objectName: "color07"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[7]; }
                            MenuItem { text: qsTr("Teal"); objectName: "color08"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[8]; }
                            MenuItem { text: qsTr("Green"); objectName: "color09"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[9]; }
                            MenuItem { text: qsTr("Light Green"); objectName: "color10"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[10]; }
                            MenuItem { text: qsTr("Lime"); objectName: "color11"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[11]; }
                            MenuItem { text: qsTr("Yellow"); objectName: "color12"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[12]; }
                            MenuItem { text: qsTr("Amber"); objectName: "color13"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[13]; }
                            MenuItem { text: qsTr("Orange"); objectName: "color14"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[14]; }
                            MenuItem { text: qsTr("Deep Orange"); objectName: "color15"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[15]; }
                            MenuItem { text: qsTr("Brown"); objectName: "color16"; icon.name: "rectangle-shape"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorMenu.colorList[16]; }


                            MenuItem { text: qsTr("Select..."); onTriggered: colorDialog.visible = true; icon.name: "color-picker"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }
                        }
                        
                        Menu {
                            id: colorFillMenu
                            objectName: "colorFillMenu"
                            
                            MenuItem { text: qsTr("Transparent"); objectName: "defaultFill";  icon.name: "edit-clear-all"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }

                            property var colorList: ["#EF9A9A", "#F48FB1", "#CE93D8", "#B39DDB", "#9FA8DA", "#90CAF9", "#81D4FA", "#80DEEA", "#80CBC4", "#A5D6A7", "#C5E1A5", "#E6EE9C", "#FFF59D", "#FFE082", "#FFCC80", "#FFAB91", "#BCAAA4"]

                            MenuItem { text: qsTr("Red"); objectName: "fill00"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[0]; }
                            MenuItem { text: qsTr("Pink"); objectName: "fill01"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[1]; }
                            MenuItem { text: qsTr("Purple"); objectName: "fill02"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[2]; }
                            MenuItem { text: qsTr("Deep Purple"); objectName: "fill03"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[3]; }
                            MenuItem { text: qsTr("Indigo"); objectName: "fill04"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[4]; }
                            MenuItem { text: qsTr("Blue"); objectName: "fill05"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[5]; }
                            MenuItem { text: qsTr("Light Blue"); objectName: "fill06"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[6]; }
                            MenuItem { text: qsTr("Cyan"); objectName: "fill07"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[7]; }
                            MenuItem { text: qsTr("Teal"); objectName: "fill08"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[8]; }
                            MenuItem { text: qsTr("Green"); objectName: "fill09"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[9]; }
                            MenuItem { text: qsTr("Light Green"); objectName: "fill10"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[10]; }
                            MenuItem { text: qsTr("Lime"); objectName: "fill11"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[11]; }
                            MenuItem { text: qsTr("Yellow"); objectName: "fill12"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[12]; }
                            MenuItem { text: qsTr("Amber"); objectName: "fill13"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[13]; }
                            MenuItem { text: qsTr("Orange"); objectName: "fill14"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[14]; }
                            MenuItem { text: qsTr("Deep Orange"); objectName: "fill15"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[15]; }
                            MenuItem { text: qsTr("Brown"); objectName: "fill16"; icon.name: "draw-circle"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; icon.color: colorFillMenu.colorList[16]; }


                            MenuItem { text: qsTr("Select..."); onTriggered: colorFillDialog.visible = true; icon.name: "color-picker"; icon.height: 16; icon.width: 16; font.family: "Open Sans"; }
                        }

                        Menu {
                            id: underlineMenu

                            MenuItem { text: "No underline"; objectName: "underline0"; font.family: "Open Sans";}
                            MenuItem { text: "_________";   objectName: "underline1"; }
                            MenuItem { text: "___ ___ __";  objectName: "underline2"; }
                            MenuItem { text: "_ _ _ _ _ _"; objectName: "underline3"; }
                            MenuItem { text: "___ _ ___ _"; objectName: "underline4"; }
                            MenuItem { text: "___ _ _ ___"; objectName: "underline5"; }
                            MenuItem { text: "~~~~~~~";     objectName: "underline6"; }
                        }

                        Menu {
                            id: letterSpacingMenu

                            MenuItem { text: "100"; objectName: "lspacing100"; }
                            MenuItem { text: "115"; objectName: "lspacing115"; }
                            MenuItem { text: "150"; objectName: "lspacing150"; }
                            MenuItem { text: "200"; objectName: "lspacing200"; }
                        }

			Menu {
                            id: paraSpacingMenu

                            MenuItem { text: "100"; objectName: "spacing0"; }
                            MenuItem { text: "115"; objectName: "spacing1"; }
                            MenuItem { text: "150"; objectName: "spacing2"; }
                            MenuItem { text: "200"; objectName: "spacing3"; }
                            MenuItem { text: "250"; objectName: "spacing4"; }
                        }
                        
                        Menu {
                            id: indentMenu

                            MenuItem { text: "Increase"; icon.name: "format-indent-more"; }
                            MenuItem { text: "Decrease"; icon.name: "format-indent-less"; }
                        }



                        Menu {
                            id: listNumberingMenu
                            
                            MenuItem { text: "Bullet"; objectName: "numbering0"; }
                            MenuItem { text: "White Bullet"; objectName: "numbering1"; }
                            MenuItem { text: "Square Bullet"; objectName: "numbering2"; }
                            MenuItem { text: "1. 2. 3."; objectName: "numbering3"; }
                            MenuItem { text: "A. B. C."; objectName: "numbering4"; }
                            MenuItem { text: "a. b. c."; objectName: "numbering5"; }
                            MenuItem { text: "I. II. III."; objectName: "numbering6"; }
                            MenuItem { text: "i. ii. iii."; objectName: "numbering7"; }
                        }
                        
                        Menu {
                            objectName: "styleMenu"
                            id: sm

                            property var styles: ["Normal"]

                            property var stylev: ["Nominal"]
                            
                            Repeater {
                                objectName: "styleRepeater"
                                id: styleRepeater
                                model: sm.styles.length
                                MenuItem {
                                    parent: styleRepeater

                                    property var fontFamily: "Open Sans";
                                    property var fontPointSize: 11;

                                    font.family: fontFamily;
                                    font.pointSize: fontPointSize;

                                    text: sm.stylev[index]; objectName: sm.styles[index];
//                                    hoverEnabled: true

                                    implicitHeight: 50

//                                    RoundButton { objectName: sm.styles[index]+"-refresh";
//                                         anchors.verticalCenter: parent.verticalCenter
//                                         icon.name: "view-refresh"; visible: parent.hovered; icon.width: 16; icon.height: 16; anchors.right: parent.right }

                                }
                            }
                        }
                        
                        Item { width: 1 }
                        
                        O20BC.RibbonMenuButton { id: pasteButton; icon.name: "special_paste"; menu: contextMenu; }
                        O20OC.Line {}
                        
                       
 
                        O20BC.ComboBox {
                            id: fontFamilySelect
                            objectName: "fontFamilySelect"; model: Qt.fontFamilies(); implicitWidth: 200;
                            Layout.leftMargin: 5; Layout.rightMargin: 2.5

                            delegate: ItemDelegate {
                                width: fontFamilySelect.width
                                text: modelData
                                font.family: modelData
//                                highlighted: fontFamilySelect.highlightedIndex === index
                            }
                        }
                        
                        O20BC.ComboBox {
                            Layout.leftMargin: 2.5; Layout.rightMargin: 5
                            implicitWidth: 80;
                            objectName: "fontSizeSelect"
                            model: [2, 5, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 24, 26, 28, 32, 48, 72]
                            onAccepted: {
                                if (find(editText) === -1)
                                    model.push(editText)
                            }
                        }
                        
                        O20OC.Line {}
                        
                        O20BC.RibbonButtonCheckable { objectName: "formatBold"; icon.name: "format-text-bold";}
                        O20BC.RibbonButtonCheckable { objectName: "formatItalic"; icon.name: "format-text-italic"; }
                        O20BC.RibbonMenuButton { objectName: "formatUnderline"; icon.name: "format-text-underline"; menu: underlineMenu; }
                        O20BC.RibbonButtonCheckable { objectName: "formatStrikeout"; icon.name: "format-text-strikethrough"; }
			O20BC.RibbonButtonCheckable { objectName: "formatOverline"; icon.source: "../../Icons/sc_overline.svg"; } 
                        
                        O20BC.RibbonButtonCheckable { objectName: "formatSuperscript"; icon.name: "format-text-superscript"; }
                        O20BC.RibbonButtonCheckable { objectName: "formatSubscript"; icon.name: "format-text-subscript"; }

                        O20BC.RibbonMenuButton { id: colorButton; objectName: "selectColorButton"; icon.name: "format-text-color"; property var color: "#ffffff"; icon.color: color; menu: colorMenu; }
                        O20BC.RibbonMenuButton { icon.name: "format-text-capitalize"; enabled: false; }
			O20BC.RibbonMenuButton { icon.name: "text_letter_spacing"; menu: letterSpacingMenu; }
                        O20BC.RibbonButton { objectName: "eraseFormat"; icon.name: "draw-eraser"; }
                        
                        O20OC.Line {}
                        
                        O20BC.RibbonButtonCheckable { objectName: "alignLeft";    icon.name: "format-justify-left"; autoExclusive: true; }
                        O20BC.RibbonButtonCheckable { objectName: "alignCenter";  icon.name: "format-justify-center"; autoExclusive: true; }
                        O20BC.RibbonButtonCheckable { objectName: "alignRight";   icon.name: "format-justify-right"; autoExclusive: true; }
                        O20BC.RibbonButtonCheckable { objectName: "alignJustify"; icon.name: "format-justify-fill"; autoExclusive: true; }
                        O20BC.RibbonMenuButton {
                            id: indentButton;
                            enabled: false
                            icon.name: "format-indent-more";
                            menu: indentMenu;
                        }
                        O20BC.RibbonMenuButton { id: spacingButton; icon.name: "text_line_spacing"; menu: paraSpacingMenu; }
                        O20BC.RibbonMenuButton { id: listNumberingButton; objectName: "listNumberingButton"; icon.name: "format-list-unordered"; menu: listNumberingMenu; }
                        
                        O20BC.RibbonMenuButton { id: colorFill; objectName: "selectFillButton"; icon.name: "color-fill"; menu: colorFillMenu;
                                                 property var color: "#ffffff"; icon.color: color; }
                        
                        O20OC.Line {}
                        
                        O20BC.RibbonMenuButton {
                            enabled: false;
                            id: styleButton;
                            icon.name: "playlist-generator"; menu: sm; 
                        }
                        
                        
                        Item { width: 10 }
                        Item { Layout.fillWidth: true }
                    }
                } // O.o

                RowLayout {
                        spacing: 2
                        Item { width: 1 }
                        O20BC.RibbonButton { objectName: "insertPageBreak"; icon.source: "../../Icons/insert-page-break"; }
			O20BC.RibbonButton { objectName: "insertAuthorName"; icon.name: "username-copy"; }
                        O20BC.RibbonButton { icon.name: "insert-image"; enabled: false; }
                        O20BC.RibbonButton { icon.name: "insert-link"; enabled: false; }
                        Item {Layout.fillWidth: true }
               }
            }
        }
    }
}
//}
