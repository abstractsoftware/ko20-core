import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtQuick.Controls.Material 2.12

import "Dialogs" as O20DC

import "o20Scripting.js" as JS

SwipeView {
    id: view

    objectName: "swipe"

    signal pageChanged(int page)

    property var myInitials: homePage.initialsButton.text
    property var myNickname: homePage.nicknameButton.text
    property var themeColor: "#2b579a"
    property var appName: "Word"

    property var aboutQtTxt

    property var useRibbon: true
    property var useStartScreen: true
    property var darkMode: false

    currentIndex: useStartScreen ? 0 : 1
    interactive: false

    font.family: "Open Sans"; font.pointSize: 10;

     O20DC.Dialog {
         id: errorDialog
         objectName: "errorDialog"
         title: qsTr("We don't support that format")
         text: qsTr("KO20 supports opening (X)HTML, ODF, DOCX, and all plain-text formats. Please try again on one of those.")
         visible: false;
     }

    //onPageChanged: {
    //    s.buttonsRef[0].checked = true
    //}

    function isDarkMode() {
        return homePage.darkModeButton.checked
    }

    function setDarkMode(a) {
        if (homePage.darkModeButton.checked != a) homePage.darkModeButton.toggle()
        darkMode = homePage.darkModeButton.checked
    }

    /*function resizeSidebar() {
       s.height = height+1;
       console.log("resizing...");
    }*/

    function restart() {
	insideScreen.opacity = 0
        fadeBack.running = true
	documentEdit = false
	s.buttonsRef[0].checked = true
    }

    Material.background: Material.theme == Material.Light ? "#f0f0f0" : "#303030";
    property var documentEdit: false

    Material.theme: {
        darkMode ? Material.Dark : Material.Light /* choose whether to use darkMaterial mode */
    }

    Material.accent: themeColor

    Component.onCompleted: {
        setDarkMode(darkMode);
    }

    Pane {
        padding: 0
        id: homeScreen

     O20DC.Dialog {
         id: fooDialog
         anchors.centerIn: parent
         text: aboutQtTxt
         visible: false;
     }

    Pane {
        id: insideScreen
        padding: 0
        anchors.fill: parent

        Pane {
           anchors.fill: parent
           Material.theme: Material.Dark
           Material.accent: "#fffff"

//      SequentialAnimation on Material.background {
//          loops: Animation.Infinite
//          ColorAnimation { to: Qt.darker(themeColor, 2); duration: 5; }
//          ColorAnimation { to: themeColor; duration: 5; }
//      }

           objectName: "loadingScreen"; Material.background: themeColor; visible: false; z: 10000;
           ColumnLayout {
              anchors.centerIn: parent
              Item { Layout.fillHeight: true; }
              Label { text: qsTr("Loading document ;)"); font.family: "Open Sans"; font.weight: Font.Light; font.pointSize: 22; Layout.fillWidth: true; height: 100; 
                      horizontalAlignment: Text.AlignHCenter; }
              Label { objectName: "documentNameToOpen"; text: "Document1.odt"; horizontalAlignment: Text.AlignHCenter; }
              Item { Layout.fillHeight: true; }
              ProgressBar {
                 implicitHeight: 10
                 Layout.fillWidth: true
                 indeterminate: true
              }
           }
        }

     SequentialAnimation on opacity {
         id: fadeBack
         running: false
         NumberAnimation { to: 1; duration: 2000 }
     }


        Layout.fillWidth: true
        Layout.fillHeight: true

        RowLayout {
            spacing: 0

            anchors.fill: parent

            O20Sidebar {
                id: s

                z: 1000
                Layout.margins: 5
                Layout.fillHeight: true
                implicitWidth: documentEdit ? 120/2 : 120
            }

            O20HomePage {
                id: homePage
                buttons: s.buttonsRef

                //x: s.sW
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
    }

    O20Editor { id: editor; height: view.height; width: view.width; Component.onCompleted: { focus: true }  }

    QtObject {
        id: qtobject
        property int index: view.currentIndex

        Behavior on index {
            ScriptAction {
               script: { pageChanged(qtobject.index) }
            }
        }
    }
}
