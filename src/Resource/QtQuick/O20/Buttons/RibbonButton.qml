import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

ToolButton {
    //icon.color: "transparent"
    icon.width: 16
    icon.height: 16

    focusPolicy: Qt.NoFocus

    //property var iconSource: ""
    //icon.source: "../../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/"+iconSource
    smooth: true

    //Material.accent: themeColor

    //hoverEnabled: true
    implicitWidth: 48
    implicitHeight: 48
}
