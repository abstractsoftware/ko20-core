import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.impl 2.12

Button {
    id: control

    property var iconSource: ""

//    font.family: "Open Sans"
//    font.pointSize: 10
    font.bold: false
    font.capitalization: Font.MixedCase

    //horizontalAlignment: Text.AlignLeft
    implicitHeight: 64
    implicitWidth: 300

    //icon.source: "../../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/"+iconSource

    icon.width: 32
    icon.height: 32

    contentItem: IconLabel {
        readonly property real arrowPadding: control.subMenu && control.arrow ? control.arrow.width + control.spacing : 0
        readonly property real indicatorPadding: control.checkable && control.indicator ? control.indicator.width + control.spacing : 0
        leftPadding: !control.mirrored ? indicatorPadding : arrowPadding
        rightPadding: control.mirrored ? indicatorPadding : arrowPadding

        spacing: control.spacing
        mirrored: control.mirrored
        display: control.display
        alignment: Qt.AlignLeft

        icon: control.icon
        text: control.text
        font: control.font
        color: control.enabled ? control.Material.foreground : control.Material.hintTextColor
    }
}
