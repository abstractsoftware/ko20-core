import QtQuick.Controls 2.12

RibbonButton {
    id: ribbonmenubutton
    property var menu

    onClicked: {
        menu.width = 150
        menu.popup();
    }

    onActiveFocusChanged: {
        if (!menu.focus) menu.dismiss();
    }
}
