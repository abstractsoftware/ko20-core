import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

ToolButton {
    id: sidebarButton

    Material.background: themeColor
    Material.accent: "white"

    property var small: false
    property var bz: "home"

    //display: small ? AbstractButton.IconOnly : AbstractButton.TextUnderIcon

    implicitHeight: sidebar.sW 
    implicitWidth: small ? sidebar.smallButtonWidth : sidebar.sW

//    icon.source: small ? "qrc:/QtQuickComponents/o20.icons.breeze/breeze.actions.16/"+bz+".svg" : "qrc:/QtQuickComponents/o20.icons.breeze/breeze.actions.32/"+bz+".svg"
    icon.color: "#ffffff"
    icon.width: small ? 16 : 32
    icon.height: small ? 16 : 32

    checkable: true
    autoExclusive: true

    hoverEnabled: false
}
