import QtQuick 2.12
import QtQuick.Controls 2.12

/* Superclass for all O20 Buttons
   Used on the Start Screen for the New and Upload buttons. */

Button {
    id: o20Button

    property var tip: ""

    implicitWidth: 120
    implicitHeight: 100

//    font.family: "Open Sans"
//    font.pointSize: 10
    font.capitalization: Font.MixedCase

    property var squareIconSize: 32

    //icon.color: "transparent"
    icon.width: squareIconSize
    icon.height: squareIconSize

    hoverEnabled: false; //tip != "";
//    ToolTip.visible: tip != "" ? hovered : false
//    ToolTip.text: tip
}
