import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Pane {
      id: pane      

      padding: 0

      Material.elevation: 6
      visible: false
      width: 160
      height: model.length > 2 ? 109 : (109-30)
      z: 1000

      property var button
      property var dG: "";
      property var model: Qt.fontFamilies()
      property var iconModel: [""]
 
      ListView {

      anchors.fill: parent

      clip: true
      model: pane.model

      delegate: MenuItem {
          id: menuItem_
          objectName: pane.dG + index
          text: modelData
          width: parent.width
          font.bold: false
          //checkable: true
          icon.name: iconModel[index] ? iconModel[index] : ""
          onClicked: /*console.log("clicked:", modelData),*/ pane.dismiss()
      }
      
      ScrollIndicator.vertical: ScrollIndicator { }
  }  
     
      function dismiss() { visible = false; }
      function popup(xx, yy) { x = xx; y = button.mapToGlobal(button.x, button.y).y - pane.height + 10; console.log(button.y); visible = true; }

      Keys.onPressed: {
          if (event.key == Qt.Key_Escape) dismiss();
      }
  }
