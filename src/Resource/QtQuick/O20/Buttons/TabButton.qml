import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

TabButton {
	id: control

	//implicitWidth: 62;
	//implicitHeight: 42;

        focusPolicy: Qt.NoFocus

	topPadding: 5
	bottomPadding: 5
	leftPadding: 5
	rightPadding: 5

        font.weight: Font.Normal
        font.capitalization: Font.MixedCase

/*	contentItem: Label {
		font.capitalization: Font.MixedCase 
		//font.family: "Open Sans"
		//font.pointSize: 10

                color: Material.theme == Material.Dark ? "#ffffff" : "#000000"

		horizontalAlignment: Text.AlignHCenter
		verticalAlignment: Text.AlignVCenter
		text: control.text
	} */
}
