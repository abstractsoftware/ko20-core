import QtQuick.Controls 2.12

RoundButton {
property var tipText;
hoverEnabled: false

focusPolicy: Qt.NoFocus

id: bob

ToolTip {
//visible: bob.visible && tipText.length > 0
font.family: "Open Sans"
font.pointSize: 10
font.bold: false
text: tipText;

x: 50
y: bob.y

visible: parent.hovered;
}

implicitWidth: 42; implicitHeight: 42; icon.width: 16; icon.height: 16;
}
