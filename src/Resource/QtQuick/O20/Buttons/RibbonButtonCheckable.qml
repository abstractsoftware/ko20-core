import QtQuick 2.12
import QtQuick.Controls 2.12

RibbonButton {
    id: ribbonButtonCheckableBase
/*    background: Rectangle {
        radius: 5
        color: checked ? "#e1dfdd" : (hovered ? "#ecebe8" : "transparent")
    } */

    checkable: true

    signal hovering(int hover)
    
    QtObject {
        id: qtobject 
        property int hovered: ribbonButtonCheckableBase.hovered
        
        Behavior on hovered {
            ScriptAction {
                script: ribbonButtonCheckableBase.hovering(qtobject.hovered)
            }
        }
    }
}
