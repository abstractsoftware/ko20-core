import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs  1.2
import QtQuick.Layouts  1.12

import "Buttons" as O20BC
import "Dialogs" as O20DC
import "Other"   as O20OC

import "o20Scripting.js" as JS

StackLayout {
    //    orientation: Qt.Vertical
    
    id: screens
    
    property var buttons; /* these are the sidebar buttons */

    property var initialsButton: initials
    property var nicknameButton: nickname 
    property var darkModeButton: useDarkMode
    
    currentIndex: {
        if (buttons[0].checked) 0
            else if (buttons[1].checked) 1
                else if (buttons[2].checked) 2
                    else if (buttons[3].checked) 3
                        else if (buttons[4].checked) 4
                            else if (buttons[5].checked) 5
    }
    
    Layout.margins: 5 
    
    //   padding: 5
    
    ColumnLayout {
        Layout.margins: 5       
        
        
        O20OC.PageHeading {
            property var greetingsList: [qsTr("Hello."), qsTr("Hi!"), qsTr("Let's get started."), qsTr("Welcome."), qsTr("Greetings."), qsTr("Hey there")]
            id: spectrumEnterGreeting; txt: greetingsList[Math.floor(Math.random(0,1)*greetingsList.length)]; showLine: false; size: 14;
        }
        
        RowLayout {
            
            ColumnLayout {
                spacing: 0
                
                O20BC.Button {
                    objectName: "createNew"
                    icon.name: "list-add"
                }
                
                O20BC.Button {
                    objectName: "upload"
                    icon.name: "document-open"
                }
                
                Item { Layout.fillHeight: true }     
                
                RowLayout {
                   
                    RoundButton {
                        objectName: "exitButton"
                        icon.width: 32; icon.height: 32; implicitWidth: 56;
			            icon.name: "dialog-close"
		            }


                    RoundButton {
                        objectName: "readOnlyButton"
                        checkable: true;
                        
                        icon.width: 32
                        icon.height: 32
                        
                        implicitWidth: 56
                        implicitHeight: 56
                        
                        icon.color: !checked ? (Material.theme == Material.Dark ? "white" : "black") : "white";  
                        icon.name: {
                            checked ? "lock" : "unlock"
                        }
                    }
                }
            }
            
            Item { width: 5 }
            
            Pane {
                
                Layout.fillWidth: true
                
                Layout.fillHeight: true
                
                //Material.elevation: 2
                
                ListView {
                    
                    objectName: "recentFilesList";
                    id: devil
                    anchors.fill: parent
                    clip: true
                    
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    
                    signal recentFileOpen; 
                    property var listIndex: -1;
                    
                    property var iconModel: [""]
                    model: [""]
                    
                    delegate: ItemDelegate {
                        text: modelData
                        width: devil.width
                        objectName: "recentFilesDelegate"
                        font.pointSize: 10; font.weight: Font.Normal;
                        icon.name: devil.iconModel[index]; icon.width: 22; icon.height: 22; icon.color: "transparent"
                        onDoubleClicked: devil.listIndex = index, devil.recentFileOpen();
                    }
                    
                    ScrollIndicator.vertical: ScrollIndicator { }
                }
                
            }
        }
    }
    
    ColumnLayout {
        Layout.margins: 5
        //            anchors.topMargin: 92-50
        
        O20OC.PageHeading { txt: qsTr("New") }
        O20OC.Template { objectName: "createNewBlank"; txt: qsTr("Blank Document"); visible: false; }
        
        Flickable {
            Layout.fillWidth: true
            Layout.fillHeight: true
           
            boundsMovement: Flickable.StopAtBounds 
            contentX: 0; contentY: 0;
            contentWidth: templatesGrid.width; contentHeight: templatesGrid.height
            
            clip: true
            ScrollBar.vertical: ScrollBar { interactive: false; }
            
            GridLayout {
                id: templatesGrid
                width: parent.width
                
                columns: Math.floor(screens.width/(152+100+32))
                
                columnSpacing: 20
                
                O20OC.Template { objectName: "singleSpaced"; txt: qsTr("Single Spaced"); icn: "../../../Templates/pic/LT02786999.png" }
                O20OC.Template { objectName: "generalNotes"; txt: qsTr("General Notes"); icn: "../../../Templates/pic/mw00002138.png" }
                O20OC.Template { objectName: "basicBlank";   txt: qsTr("Basic Blank"); icn: "../../../Templates/pic/LT16392878.png" }
                O20OC.Template { objectName: "businessLetter"; txt: qsTr("Business Letter"); icn: "../../../Templates/pic/mw00002133.png" }
                O20OC.Template { objectName: "boldReport"; txt: qsTr("Bold Report"); icn: "../../../Templates/pic/mw00002101.png" }
                O20OC.Template { objectName: "studentReport"; txt: qsTr("Student Report"); icn: "../../../Templates/pic/mw00002090.png"}
                O20OC.Template { objectName: "resume"; txt: qsTr("Resume"); icn: "../../../Templates/pic/mw00002024.png"}
                O20OC.Template { objectName: "resumeCoverLetter"; txt: qsTr("Resume Cover Letter"); icn: "../../../Templates/pic/mw00002097.png" }
            }
        }
    }
    
    ColumnLayout {
        Layout.margins: 5
        //            anchors.topMargin: 92-50
        
        O20OC.PageHeading { txt: qsTr("Open") }
        
        RowLayout {
            
            ColumnLayout { 
                O20BC.PushButton {
                    objectName: "browseDisk"
                    icon.name: "drive-harddisk-symbolic"
                    text: qsTr("Browse")
                }
                
                O20BC.PushButton {
                    objectName: "openCode"
                    icon.name: "view-services-scripted-amarok"
                    text: qsTr("Import Code")
                }
                
                O20BC.PushButton {
                    objectName: "openLocation"
                    icon.name: "edit-paste-in-place";
                    text: qsTr("Display Location")
                }
                
                Item { Layout.fillHeight: true }
                
            }
            
            Pane {
                
                Layout.fillWidth: true
                
                Layout.fillHeight: true
                
                //Material.elevation: 2
                
                ListView {
                    
                    objectName: "internalFileManager";
                    id: diabolo
                    anchors.fill: parent
                    clip: true
                    
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    
                    signal itemClicked();
                    
                    property var currentItem: ""
                    
                    property var iconModel: ["folder", "text-x-python"]
                    model: ["Jajaja", "DootDoot.py"]
                    
                    delegate: ItemDelegate {
                        text: modelData
                        width: diabolo.width
                        objectName: "recentFilesDelegate"

                        property bool relativeIcon: diabolo.iconModel[index].substring(0, 5) != "qrc:/"

                        font.pointSize: 10; font.weight: Font.Normal;
                        icon.name:   relativeIcon ? diabolo.iconModel[index] : ""; 
                        icon.source: relativeIcon ? "" : diabolo.iconModel[index];
                        icon.width: 22; icon.height: 22;

                        icon.color: (diabolo.iconModel[index] == "desktop" ||
                                     diabolo.iconModel[index].match("folder")) ?
                                    (Material.theme == Material.Dark ? "#ffffff" : "#000000") : "transparent";
                        onDoubleClicked: diabolo.currentItem = text, diabolo.itemClicked();
                    }
                    
                    ScrollIndicator.vertical: ScrollIndicator { }
                }
                
            }
            
        }
    }
    
    ColumnLayout {
        Layout.margins: 5
        //            anchors.topMargin: 92-50
        
        O20OC.PageHeading { txt: qsTr("Save") }
        
        O20BC.PushButton {
            objectName: "documentSaveAs";
            icon.name: "document-save-as";
            text: qsTr("Save As")
        }
        
        O20BC.PushButton {
            objectName: "documentSaveAsPDF"
            icon.source: "../../Icons/acrobat.svg";
            text: qsTr("Export to PDF")
        }
        
        O20BC.PushButton {
            id: renameDocument
            
            icon.name: "cursor-arrow"
            text: qsTr("Rename")
            
            checkable: true
        }
        
        TextField {
            objectName: "renameDocument"
            visible: renameDocument.checked
            implicitWidth: 300
            placeholderText: qsTr("Rename document to...?")
            onAccepted: renameDocument.checked = false
            Keys.onPressed: if (event.key == Qt.Key_Escape) renameDocument.checked = false
        }
        
        O20BC.PushButton {
            objectName: "documentPrint"
            icon.name: "document-print"
            text: "Print"
        }
        
        DelayButton {
            objectName: "trashDocument"
            icon.name: "trash-empty"
            delay: 3000
            font.family: "Open Sans"; font.pointSize: 10;
            text: qsTr("Delete")
        }
        
        DelayButton {
            objectName: "closeDocument"
            delay: 1000
            font.family: "Open Sans"; font.pointSize: 10;
            text: qsTr("Close")
        }
        
        
        Item { Layout.fillHeight: true }
    }
    
    ColumnLayout {
        Layout.margins: 5
        //anchors.margins: 5
        //            anchors.topMargin: 92-50
        
        spacing: 10
        O20OC.PageHeading { txt: qsTr("About"); }

//        RowLayout {
//           Image { source: "../../O20Core/apps/io.gitlab.ko20.ink.svg"; }
           Label { text: "KO20 Version2"; font.pointSize: 18; }
//        }

        RowLayout {
            Layout.fillWidth: true
            //width: 500
            spacing: 10
            
             ColumnLayout {

                O20BC.PushButton {
                    objectName: "aboutQt"

                    //implicitWidth: 56
                    //implicitHeight: 56

                    text: qsTr("About KDE and Qt")
                    onClicked: fooDialog.visible = true
                    icon.name: "kdeapp"
                    icon.color: "transparent"                    
                }

                O20BC.PushButton {
                    onClicked: Qt.openUrlExternally("https://abstractsoftware.gitlab.io")

                    text: qsTr("About Us")
                    icon.name: "kolourpaint"
                    icon.color: "transparent"
                }

                O20BC.PushButton {
                    onClicked: Qt.openUrlExternally("https://gitlab.com/abstractsoftware/ko20-core")

                    text: qsTr("Get the code")
                    icon.name: "applications-development"
                    icon.color: "transparent"
                }

            //}

            TextArea {
                width: 500
                readOnly: true;
                background: Pane {}
                font.family: "Open Sans"
                font.pointSize: 11
                text: "A free and open source office suite written in QtQuick with a modern design based on Office 365 standards.\nCopyright (C) 2020 Abstract Developers\n\n"
                      //"KO20 is a minimal office suite written in QtQuick and C++, with a modern UI/UX, Google Material controls, ODT support, " +
                      //"and syntax highlighting for over 100 programming languages.\n" +
                      //"Our word processor, text editor, and MS Word alternative, KO20.Ink, is the perfect app for writing code, drafting novels, and creating simple essays " +
                      //"on your Linux desktop."

                wrapMode: TextEdit.Wrap
            }

            }

        }
    }
    
    ColumnLayout {
        
        //            anchors.margins: 5
        //            anchors.margins: 5
        //          anchors.topMargin: 92-50
        
        id: settingsCol
        
        spacing: 10 
        O20OC.PageHeading { txt: qsTr("Options"); }
        
//        O20OC.DesktopScroll {
//            id: don
//            Layout.fillWidth: true; Layout.fillHeight: true;
            
//            padding: 3 
            //                topPadding: 0
            
            ColumnLayout {
                width: 500
                spacing: 10
                
                /*Pane {
                    Layout.fillWidth: true;
                    
                    Material.background: Material.theme == Material.Light ? "#ffffff" : "#101010";
                    Material.elevation: 2;
                    
                    ColumnLayout {
                        anchors.fill: parent
                        Label {
                            padding: 8;
                            text: qsTr("General Settings"); font.pointSize: 14;
                        }
                        
                        CheckBox { LayoutMirroring.enabled: true; Layout.fillWidth: true; text: qsTr("Recover the window state") }
                        CheckBox { LayoutMirroring.enabled: true; Layout.fillWidth: true; text: qsTr("Show the splash screen") }
                        CheckBox { LayoutMirroring.enabled: true; Layout.fillWidth: true; text: qsTr("Show the start screen") }
                    }
                }*/
                
                Pane {
                    implicitWidth: 500
                    
                    Material.background: Material.theme == Material.Light ? "#ffffff" : "#101010";
                    Material.elevation: 2;
                    
                    ColumnLayout {
                        anchors.fill: parent;
                        Label {
                            padding: 8;
                            text: qsTr("User Customization"); font.pointSize: 14;
                        }
                        
                        TextField { objectName: "nickname"; id: nickname;
                                    placeholderText: qsTr("What would you like me to call you?"); Layout.fillWidth: true; Layout.leftMargin: 8; }
                        TextField { objectName: "fullname"; placeholderText: qsTr("What is your full name?"); Layout.fillWidth: true; Layout.leftMargin: 8; }
                        TextField { objectName: "initials"; id: initials;
                                    placeholderText: qsTr("What are your initials?"); Layout.fillWidth: true; Layout.leftMargin: 8; }

                       // ListView {
                       //     Layout.fillWidth: true
                       //     model: ["Use Dark Mode"]
                            /*delegate:*/ SwitchDelegate {
                                 id: useDarkMode;
                                 objectName: "useDarkMode";
                                 Layout.fillWidth: true;
                                 text: qsTr("Use Dark Mode")
                                 onToggled: darkMode = useDarkMode.checked
                            }
                       //}

//                        RowLayout {
//                            Layout.leftMargin: 8;
//                            Label { text: qsTr("Use Dark Mode"); Layout.fillWidth: true; }
//                            Switch { objectName: qsTr("useDarkMode"); id: useDarkMode; onToggled: darkMode = useDarkMode.checked }
//                        }
                    }
                }
                
                /*                Pane {
                 *                    Layout.fillWidth: true;
                 *                    
                 *                    Material.background: Material.theme == Material.Light ? "#ffffff" : "#101010";
                 *                    Material.elevation: 2;
                 *                    
                 *                    ColumnLayout {
                 *                        anchors.fill: parent
                 *                        Label {
                 *                            padding: 8;
                 *                            text: qsTr("Save and AutoSave"); font.pointSize: 14;
            }
            
            CheckBox { LayoutMirroring.enabled: true; Layout.fillWidth: true; text: qsTr("Always AutoSave") }
            CheckBox { LayoutMirroring.enabled: true; Layout.fillWidth: true; text: qsTr("Ask for save location") }
            }
            } */
                
            }
        //}
    }
}
