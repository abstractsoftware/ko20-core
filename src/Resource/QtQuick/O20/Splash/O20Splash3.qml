import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12

Pane {
    visible: true
    
    //anchors.fill: parent
    
    //radius: 5
    
    width: 400
    height: 200
   
    property var gallery: false
 
    Material.accent: appColor
    Material.theme: Material.Dark
    
    Material.elevation: 20
    
    //    background: Rectangle { radius: 5; color: Material.theme == Material.Light ? "#fafafa" : "#303030"}
    
    property var useNewSplash: true
    
    property string appColor: "#e83f24"
    property string appIcon: "../../../O20Core/apps/ms-office.svg" 
    property string qversion: "5.13.2"
    property string appName: "O20.App"
    
    SequentialAnimation on Material.background {
        objectName: "splashAnimation"
        loops: Animation.Infinite
        running: false
        ColorAnimation { to: Qt.darker(appColor, 2); duration: 500; }
        ColorAnimation { to: "#303030"; duration: 500; }
    }
    
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        
        Rectangle {
            Layout.fillWidth: true
            height: 16
            color: "transparent"
            RowLayout {
                anchors.fill: parent
                Item { Layout.fillWidth: true; }
                ToolButton {
                    icon.name: "window-close"
                    //icon.color: "#ffffff"
                    height: 16
                    width: 16
                    padding: 2
                    onClicked: {
                        Qt.quit();
                    }
                } 
            }
        }
        
        Item { Layout.fillHeight: true }
        
        Rectangle {
            Layout.fillWidth: true
            
            height: 66
            color: "transparent"
            
            RowLayout {
                anchors.fill: parent
                
                Item { Layout.fillWidth: true; }
                
                Rectangle {
                    visible: false;
                    height: 86
                    width: 86
                    //Layout.fillWidth: true
                    color: "transparent"
                    
                    Image {
                        visible: false
                        id: appImage
                        anchors.centerIn: parent
                        source: appIcon
                        z: 500
                    }
                    
                }
                
                Item { width: 10; visible: false; }
                
                Label {
                    //Layout.fillWidth: true
                    textFormat: Text.RichText
                    text: appName
                    //color: "white"
                    font.pointSize: 22
                    font.family: "Open Sans"
                    font.weight: Font.Light
                    horizontalAlignment: Text.AlignHCenter
                }
                
                Item { Layout.fillWidth: true; }
                
                /*ToolButton { 
                 *               icon.source: "../BreezeDark/window-close.svg"; icon.color: "#ffffff"; 
                 *               onClicked: {
                 *                   Qt.quit()
            }
            }*/
            }
        }
        
        Label { objectName: "splashTip"; font.family: "Open Sans"; font.pointSize: 10; horizontalAlignment: Text.AlignHCenter; text: "Loading..."; Layout.fillWidth: true; Layout.fillHeight: true; wrapMode: Text.Wrap; }
        
        
        
        Button {
            objectName: "openInk"      
 
            visible: gallery;     
            Layout.alignment: Qt.AlignHCenter
            
            implicitWidth: 150
            implicitHeight: 48
            
            font.family: "Open Sans"; font.pointSize: 10;
            
            font.capitalization: Font.MixedCase
            text: "Launch Ink"
            
            icon.width: 22; icon.height: 22; icon.color: "transparent";
            icon.name: "application-vnd.oasis.opendocument.text"
        }
        
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
        
        ProgressBar {
            visible: true; //!useNewSplash
            //anchors.centerIn: parent
            Layout.alignment: Qt.AlignHCenter
            implicitWidth: 300
            indeterminate: true
        }
        
        Item { height: 5 }
    }
}

