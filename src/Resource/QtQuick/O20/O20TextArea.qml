import QtQuick 2.7
import QtQuick.Controls 1.7
import QtQuick.Layouts 1.7

Flickable {
    id: flick
    clip: true

    property var editor: edit

    property var textDocument: edit.textDocument
    property var ooo: Math.floor(edit.contentHeight/h)+1
    property var w: 500
    property var h: 750

    ScrollBar.vertical: ScrollBar { policy: ScrollBar.AlwaysOn }
    ScrollBar.horizontal: ScrollBar {}

    contentWidth: w; contentHeight: edit.height+50; //contentHeight: ooo * h

    anchors.margins: 40

    //anchors.leftMargin: (width - w) /2
    //anchors.rightMargin: anchors.leftMargin

    function ensureVisible(r) {
        if (contentX >= r.x)
            contentX = r.x;
       else if (contentX+width <= r.x+r.width)
            contentX = r.x+r.width-width;
       if (contentY >= r.y)
            contentY = r.y;
       else if (contentY+height <= r.y+r.height)
            contentY = r.y+r.height-height;
    }     

    /*Item {
        width: Math.max(flick.width, column.width)
        height: Math.max(flick.height, column.height)

        ColumnLayout {
            anchors.centerIn: parent

            Item { height: 50 }  */

            TextArea {
                id: edit
                //anchors.centerIn: parent
                //width: w
                //height: ooo*h

                //padding: 50

                focus: true
                selectByMouse: true
                wrapMode: TextEdit.Wrap

                /*background: Column {
                    id: column
                    //anchors.centerIn: parent
                    spacing: 0
    
                    Repeater {
                        model: ooo
      
                        Rectangle {
                            Layout.alignment: Qt.AlignHCenter
                            width: w
                            height: h
                            color: "white"
                            Text { text: index }
                            border.color: "#c6c6c6"
                            //Component.onCompleted: console.log("EH???....", index, ooo, edit.contentHeight, edit.lineCount, pageEdit.width)
                        }
                    }
                }*/

                //background: Rectangle { id: column; height: Math.max(h, edit.height); width: w; border.color: "#c6c6c6"; color: "white"; }

                onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
            }
        //}
    //}
}
