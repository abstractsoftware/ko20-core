import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

import QtQuick.Controls 1.4 as QQC1

import "Buttons" as O20BC
import "Dialogs" as O20DG
import "Other"   as O20OC

import OF20 3.0 as OF20

import "o20Scripting.js" as JS

Item {
    property var styles: ["Normal", "Title", "Subtitle", "Heading 1", "Heading 2", "Heading 3", "Emphasis", "Strong", "Quote", "Reference"]
    
    //property var editorClass: docview.editor
    //Material.theme: Material.Light
    Material.background: Material.theme == Material.Light ? "#f0f0f0" : "#303030"
    
    //    O20DG.FontDialog { id: fontDialog }
    
    O20DG.ColorDialog { id: colorDialog; objectName: "colorDialog"; }
    O20DG.ColorDialog { id: colorFillDialog; objectName: "colorFillDialog"; }
    
    id: editor
    Pane {
        anchors.fill: parent
        padding: 0;     
        //color: "#f0f0f0";
        
        ColumnLayout {
            anchors.fill: parent
            spacing: 0
            
            O20Ribbon { objectName: "ribbonAnchor"; id: ribbon; Layout.fillWidth: true; }
            
            Item { height: 5; /*visible: ribbon.tabsVisible;*/ }
            
            RowLayout {
                Layout.fillHeight: true; Layout.fillWidth: true;
                
                Material.accent: themeColor
                
                spacing: 0
                
                ColumnLayout {
                    id: col
                    spacing: 0
                    
                    OF20.DocumentItem {
                        objectName: "editor";
                        id: edit
                        Layout.fillHeight: true;
                        Layout.fillWidth: true;
                   
                        focus: true
 
                            //Timer {
                            //    interval: 200; running: true; repeat: true
                            //    onTriggered: if (view.currentIndex) edit.focus = true
                            //}
 
                        RowLayout {
                            anchors.bottom: parent.bottom; anchors.bottomMargin: 20;
                            height: 50
                            anchors.left: parent.left;
                            anchors.leftMargin: 20
                            
                            Pane { Material.elevation: 6; Label { objectName: "documentMetrics"; anchors.fill: parent; text: "Hi!"; } }
                            
                            O20BC.FloatingButton {
                                objectName: "changePosition"; checkable: true;
                                icon.name: checked ? "go-top-symbolic" : "go-bottom-symbolic"; icon.color: !checked ? (Material.theme == Material.Dark ? "white" : "black") : "white";
                            }
                            
                            //Item { Layout.fillWidth: true; }
                            
                            O20BC.FloatingButton {
                                objectName: "warningButton"
                                visible: false
                                
                                Material.background: "#FFCC80"
                                icon.name: "state-warning"
                                onReleased: visible = false
                                icon.color: transparent
                                
                                hoverEnabled: true; tipText: "WARNING: Any changes made to this document will be lost.";
                            }
                            
                            O20BC.FloatingButton {
                                objectName: "criticalErrorButton"
                                visible: false
                                
                                Material.background: "#EF9A9A"
                                icon.name: "state-error"
                                onReleased: visible = false
                                icon.color: transparent
                                
                                hoverEnabled: true; tipText: "CRITICAL ERROR: We couldn't open this document.";
                            }
                            
                        }
                        
                        RowLayout {
                            
                            anchors.bottom: parent.bottom; anchors.bottomMargin: 20; anchors.right: parent.right; anchors.rightMargin: 30
                            
                            O20BC.FloatingButton { objectName: "checkSpelling"; Material.accent: "#4CAF50"; icon.name: "tools-check-spelling"; checkable: true;
                                icon.color: !checked ? (Material.theme == Material.Dark ? "white" : "black") : "white"; }
                                
                                O20BC.FloatingButton {
                                    objectName: "changePageStyle"; checkable: true;
                                    icon.name: "view-pages-continuous"; icon.color: !checked ? (Material.theme == Material.Dark ? "white" : "black") : "white";
                                }
                                
                                O20BC.FloatingButton {
                                    objectName: "viewFullScreen"; checkable: true;
                                    icon.name: "view-fullscreen"; icon.color: !checked ? (Material.theme == Material.Dark ? "white" : "black") : "white";
                                }
                                
                        }
                        
                    }
                    
                    Drawer {
                        objectName: "sidebarDrawer"
                        id: sidebar

//                        Material.theme: Material.Dark
//                        Material.accent: "white"
//                        Material.background: themeColor

                        font.family: "Ubuntu"; font.pointSize: 10

			Material.accent: themeColor

                        edge: Qt.RightEdge
                        implicitWidth: 250
                        implicitHeight: editor.height
                       
                        padding: 10;
                       
                        leftMargin: 10

                        onClosed: edit.focus = true
 
                        interactive: view.currentIndex
                        visible: false; //!hideSidebar.checked
                        Material.elevation: 10
                        
                        
                        ColumnLayout {
                            id: drawerItem
                            anchors.fill: parent
                            
                            spacing: 0
                            
                            TabBar {

                                id: bar
                                Layout.fillWidth: true
                                
                                //O20BC.TabButton {
                                //    text: qsTr("Home")
                                //}
                                
                                //O20BC.TabButton {
                                //    text: qsTr("Help")
                                //}
                                
                                O20BC.TabButton {
                                    text: qsTr("Headings")
                                }

                                //O20BC.TabButton {
                                //    text: qsTr("Comments")
                                //}
                            }
                            
                            StackLayout {
                                id: drawerSwipe
                                
                                //focusPolicy: Qt.NoFocus

                                height: drawerItem.height
                                width:  250
                                
                                currentIndex: bar.currentIndex
                                //interactive: false
                                
                                Pane {
                                    implicitWidth: 250
                                    padding: 0

                                    ListView {
                                       
                                        objectName: "headingsView" 
                                        id: headingsView
                                        clip: true
                                       
                                        width: 250 
                                        anchors.fill: parent
                                        signal itemClicked();
                                        
                                        property var headingIndex: 0
                                       
                                        model: []

                                        delegate: ItemDelegate {
                                            text: modelData;
                                            focusPolicy: Qt.NoFocus
                                            width: headingsView.width; smooth: true;
                                            font.weight: Font.Normal;
                                            onClicked: headingsView.headingIndex = index, headingsView.itemClicked()
                                            //icon.name: headingsView.iconModel[index]; icon.width: 22; icon.height: 22; //icon.color: "transparent"
                                        }

                                        ScrollIndicator.vertical: ScrollIndicator { }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
}
