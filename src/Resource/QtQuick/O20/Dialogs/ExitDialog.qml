import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
   color: "green"

   Row {
   Button {
       text: "Show Dialog"
       onClicked: dialog.visible = true
   }

   BusyIndicator {
       running: true
   }
   }

Dialog {
    id: dialog

    anchors.centerIn: parent
    Material.theme: Material.Dark
    Material.accent: "white"


    title: "Yoo r a fool"
    standardButtons: Dialog.Ok | Dialog.Cancel

    visible: false
    modal: true

    onAccepted: console.log("Ok clicked")
    onRejected: console.log("Cancel clicked")
}
}
