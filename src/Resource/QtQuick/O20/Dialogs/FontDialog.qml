import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: dialog

    Material.accent: themeColor
    anchors.centerIn: parent

    modal: true

    visible: true
    property var text

    font.family: "Open Sans"; font.pointSize: 10;

    title: "Select Font"

    contentItem: Pane {
        font.family: "Open Sans"; font.pointSize: 10;

        ColumnLayout {
            RowLayout {
                
            }
        }

        anchors.fill: dialog
    }

    standardButtons: Dialog.Ok

    onAccepted: console.log("Ok clicked")
}
