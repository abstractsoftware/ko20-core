import QtQuick 2.12
import QtQuick.Layouts  1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: dialog

    property var color: a.text

//    Material.background: a.text
//    Component.onCompleted: console.log(Material.background.r);
//    Material.theme: ((Material.background.r*0.299 + Material.background.g*0.587 + Material.background.b*0.114) > 186) ? "Light" : "Dark"
    Material.accent: themeColor
    anchors.centerIn: parent

    modal: true

    visible: false
    property var text

    font.family: "Open Sans"; font.pointSize: 10;

    title: "KO20 Color Picker"

    contentItem: Item {
        ColumnLayout {
             Pane { Material.background: a.text; Layout.fillWidth: true; implicitHeight: 50; Material.elevation: 5; }


             RowLayout {
                 id: b
             GridLayout {
                 id: g
                 columns: 6;
                 property var colorList: ["#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#4CAF50", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548"]

                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[0]; 
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[1];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[2];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[3];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[4];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[5];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[6];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[7];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[8];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[9];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[10];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[11];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[12];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[13];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[14];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[15];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: g.colorList[16];
                          onReleased: a.text = Material.background }

             }

             Item { width: 10 }

             GridLayout {
                 id: e
                 columns: 6;

                property var colorList2: ["#EF9A9A", "#F48FB1", "#CE93D8", "#B39DDB", "#9FA8DA", "#90CAF9", "#81D4FA", "#80DEEA", "#80CBC4", "#A5D6A7", "#C5E1A5", "#E6EE9C", "#FFF59D", "#FFE082", "#FFCC80", "#FFAB91", "#BCAAA4"]

                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[0];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[1];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[2];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[3];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[4];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[5];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[6];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[7];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[8];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[9];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[10];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[11];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[12];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[13];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[14];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[15];
                          onReleased: a.text = Material.background }
                 Button { implicitWidth: 32; implicitHeight: 32; Material.background: e.colorList2[16];
                          onReleased: a.text = Material.background }
             }
             }

//             RowLayout {
//                 Label { text: qsTr("Original color") }
//                 Button { objectName: "originalColor"; implicitWidth: 32; implicitHeight: 32; Material.background: "#000000";
//                          onReleased: a.text = Material.background; }
//                 Item { Layout.fillWidth: true; }
//                 CheckBox { text: "Default color" }
//             }

             TextField { objectName: "line"; id: a; text: "#000000"; /*onTextChanged: dialog.Material.theme = ((dialog.Material.background.r*0.299 + dialog.Material.background.g*0.587 + dialog.Material.background.b*0.114) > 0.7) ? Material.Light : Material.Dark */}

        }
    }

    standardButtons: Dialog.Ok

 //   onAccepted: console.log("Ok clicked")
}
