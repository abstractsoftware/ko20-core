import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: dialog

    width: 500; height: 500;

    Material.accent: themeColor
    anchors.centerIn: parent

    modal: true

    visible: true
    property var text

    font.weight: Font.Light
    font.family: "Open Sans"; font.pointSize: 10;
    title: "KDE - Be Free!"

    contentItem: Label {

        text: dialog.text
        wrapMode: Text.Wrap


        font.weight: Font.Normal
//        font.family: "Open Sans"; font.pointSize: 10;
        anchors.fill: dialog
    }

    standardButtons: Dialog.Ok

    //onAccepted: console.log("Ok clicked")
}
