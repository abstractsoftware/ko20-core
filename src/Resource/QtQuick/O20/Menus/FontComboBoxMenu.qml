import QtQuick 2.12
import QtQuick.Controls 2.12

ListView {
      width: 160
      height: 240

      model: Qt.fontFamilies()

      delegate: ItemDelegate {
          text: modelData
          width: parent.width
          onClicked: console.log("clicked:", modelData)
      }

      ScrollIndicator.vertical: ScrollIndicator { }
}
