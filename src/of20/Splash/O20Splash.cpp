#include "O20Splash.h"

namespace KO20 {

	Splash::Splash(KO20::App app) {
                setSource(QUrl("qrc:/QtQuick/O20/Splash/O20Splash3.qml"));

                rootObject()->setProperty("appColor", app.color);
                rootObject()->setProperty("appIcon", app.icon);
                rootObject()->setProperty("appName", app.name);

                rootObject()->setProperty("useNewSplash", false);

                setClearColor(Qt::transparent);
                setAttribute(Qt::WA_TranslucentBackground);
                setResizeMode(QQuickWidget::SizeRootObjectToView);
                setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::ToolTip);

                QRect desktopRect = QGuiApplication::screens()[0]->availableGeometry();
                QPoint center = desktopRect.center();

                setGeometry(center.x() - 400 * 0.5, center.y() - 150 * 0.5, 400, 150);

                QObject::connect(O20_QO("openInk"), SIGNAL(clicked()), this, SLOT(launchApp()));

                QObject::connect(engine(), &QQmlEngine::quit, [=] { qApp->exit(); });
                qApp->processEvents();

//              O20_QOSET("splashTip", "visible", false);
                O20_QOSET("splashAnimation", "running", true);
            }


   bool Splash::launchApp() {
	if (sender() == O20_QO("openInk")) kApp->app = "ink";
        return kApp->init(this);
   }

};
