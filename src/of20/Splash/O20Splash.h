#ifndef O20SPLASH_H
#define O20SPLASH_H

#include <QQuickWidget>
#include <QScreen>
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQuickItem>

#include "AppCtl.h"
#include <Ink/Ink.h>
#include <O20/App.h>
#include <O20/QtQuick.h>

namespace KO20 {
    class Splash : public QQuickWidget {
        Q_OBJECT

        public:

            AppCtl *kApp;

            Splash(KO20::App app);

            void makeLargeSplash() { 
                QRect desktopRect = QGuiApplication::screens()[0]->availableGeometry();
                QPoint center = desktopRect.center();

                setGeometry(center.x() - 400 * 0.5, center.y() - 400 * 0.5, 400, 400);
	    }

	    void setGalleryMode(bool gx) { rootObject()->setProperty("gallery", gx); }
	    void setTip(QString tip) { O20_QOSET("splashTip", "text", tip); }

	public Q_SLOTS:
            bool launchApp(); // { qDebug("DUMMY"); if (sender() == this) qDebug("EYY"), kApp->app = "ink"; return kApp->init(this); }
    };
};

#endif
