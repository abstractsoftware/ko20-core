#ifndef KO20_APPCTL_H
#define KO20_APPCTL_H

#include <Ink/Ink.h>
#include <QtCore>

namespace KO20 {
    class Splash;

    class AppCtl {
        public:
           AppCtl(): ux(""), c(false), ro(false), to(false), co(false) {};

	   void setArgs(QStringList _args) { args = _args; }

	   QStringList args;
	   // ux = path to custom KrakenInterface directory
	   QString ux;
           // c  = choose document from file dialog on startup
	   // ro = is document readonly
	   // to = open all non-binary formats as code
	   // co = create the document if it doesn't exist
	   bool c, ro, to, co;
	   QString app;

	   bool init(Splash*);
    };
};

#endif
