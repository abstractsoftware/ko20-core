#include "AppCtl.h"
#include "O20Splash.h"

namespace KO20 {

    bool AppCtl::init(Splash* ss) {

        if (app == "ink") {
            KO20::Ink *app = new KO20::Ink(ux);

            if (args.size() > 1) {
                app->setReadOnly(ro);
                app->setTextOnly(to);

                app->createDocument(args.at(1), co, args.at(1));
            }

            if (c) {
                ss->setTip("Please select a document...");
                app->setReadOnly(ro);
                app->setTextOnly(to);

                if (!app->createDocument()) return 0;
            }

            ss->close();
            app->startCore();
            app->setMinimumSize(900, 600);
            app->show();
	}

	return 1;
    }
};
