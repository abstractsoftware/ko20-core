#include <KZip>
#include <QXmlStreamWriter>
#include <QTextCharFormat>
#include <QTextBlockFormat>
#include <QFile>
#include <QDebug>

#include "odtpatch.h"

namespace O20 {

   void PatchODT(QString filename, QStringList stylenames, QMap<QString, QTextCharFormat> scf, QMap<QString, QTextBlockFormat> sbf) {
        KZip zdoc(filename);
	zdoc.open(QIODevice::ReadWrite); // append files
	
	QString styleXML_string;
        QXmlStreamWriter stream(&styleXML_string); // write styles

	stream.writeStartDocument();
	stream.writeNamespace("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "office");
        stream.writeNamespace("urn:oasis:names:tc:opendocument:xmlns:style:1.0", "style");
        stream.writeNamespace("urn:oasis:names:tc:opendocument:xmlns:fo:1.0", "fo");

        stream.writeStartElement("office:document-styles");
        stream.writeAttribute("office:version", "1.2");
	stream.writeStartElement("office:styles");

        for (QString stylename : stylenames) {
           stream.writeStartElement("style:style");

           QTextCharFormat  cf = scf[stylename];
	   QTextBlockFormat bf = sbf[stylename];

	   QString stylename_ns = stylename; stylename_ns.remove(" ");
	   stream.writeAttribute("style:name", stylename_ns);
           stream.writeAttribute("style:display-name", stylename);
           stream.writeAttribute("style:family", "paragraph");

           stream.writeStartElement("style:paragraph-properties");

           QString p_ta = "left";
	   if (bf.alignment() == Qt::AlignRight) p_ta = "right";
	   else if (bf.alignment() == Qt::AlignCenter or bf.alignment() == Qt::AlignHCenter) p_ta = "center";
	   else if (bf.alignment() == Qt::AlignJustify) p_ta = "justify";

           stream.writeAttribute("fo:text-align", p_ta);

	   stream.writeEndElement(); // paragraph-properties

           stream.writeStartElement("style:text-properties");

           QString ff = cf.fontFamily();
           QString fs = QString::number(cf.fontPointSize());
	   QString fb = cf.fontWeight() == QFont::Bold ? "bold" : "";
	   QString fi = cf.fontItalic() ? "italic" : "";
	   QString fc = cf.foreground().color().name();

	   if (!ff.isEmpty() && !ff.isNull())
               stream.writeAttribute("fo:font-family", ff);

           if (!fs.isEmpty() && !fs.isNull() && fs != "0")
               stream.writeAttribute("fo:font-size", fs + "pt");

           if (!fb.isEmpty() && !fb.isNull())
               stream.writeAttribute("fo:font-weight", fb);

           if (!fi.isEmpty() && !fi.isNull())
               stream.writeAttribute("fo:font-style", fi);

           stream.writeAttribute("fo:color", fc);

	   stream.writeEndElement(); // text-properties

           stream.writeEndElement(); // style

	}

	stream.writeEndElement(); // styles
        stream.writeEndElement(); // document-styles
        stream.writeEndDocument();

        QString manifest = "<?xml version='1.0' encoding='UTF-8'?>"
        "<manifest:manifest xmlns:manifest='urn:oasis:names:tc:opendocument:xmlns:manifest:1.0' manifest:version='1.2' "
        "xmlns:loext=\"urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0\">"
        "<manifest:file-entry manifest:full-path=\"/\" manifest:version='1.2' manifest:media-type=\"application/vnd.oasis.opendocument.text\"/>"
        "<manifest:file-entry manifest:full-path=\"styles.xml\" manifest:media-type=\"text/xml\"/>"
        //"<manifest:file-entry manifest:full-path=\"Configurations2/\" manifest:media-type=\"application/vnd.sun.xml.ui.configuration\"/>"
        "<manifest:file-entry manifest:full-path=\"Thumbnails/thumbnail.png\" manifest:media-type=\"image/png\"/>"
        "<manifest:file-entry manifest:full-path='content.xml' manifest:media-type='text/xml'/>"
        //"<manifest:file-entry manifest:full-path='meta.xml' manifest:media-type='text/xml'/>"
        //"<manifest:file-entry manifest:full-path='manifest.rdf' manifest:media-type='application/rdf+xml'/>"
        //"<manifest:file-entry manifest:full-path='settings.xml' manifest:media-type='text/xml'/>"
        "</manifest:manifest>";

        QFile file(":/thumbnail.png"); //":/Templates/pic/LT02786999.png");
        file.open(QFile::ReadOnly);
        QByteArray thumbnail = file.readAll();

	zdoc.writeFile("styles.xml", styleXML_string.toUtf8());
        zdoc.writeDir("Thumbnails");
	zdoc.writeFile("Thumbnails/thumbnail.png", thumbnail);
	zdoc.writeFile("META-INF/manifest.xml", manifest.toUtf8());

	zdoc.close();
    }
}
