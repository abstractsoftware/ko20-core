#include <KZip>
#include <QXmlStreamWriter>
#include <QTextCharFormat>
#include <QTextBlockFormat>
#include <QFile>

#include <KArchiveDirectory>
#include <KZipFileEntry>

#include "openDocx.h"
#include <QDebug>

namespace O20 {
   namespace MSFormats {

       QString majorFont = "Open Sans Light";
       QString minorFont = "Open Sans";

       QMap<QString, QString> styleList;

       QString parseLocalStyles(QXmlStreamReader& stream) {
           QString spanstyle = "";
           while (stream.readNextStartElement()) {
               // b, i, sz, rFonts
               QString pname = stream.name().toString();
               QString val = stream.attributes().value("w:val").toString();

               if (pname == "b" && val != "0") spanstyle += (val == "0" ? "font-weight: normal;" : "font-weight: bold;");
               else if (pname == "i") spanstyle += (val == "0" ? "font-style: normal;" : "font-style: italic;");
               //else if (pname == "pStyle") spanstyle += styleList[val];
               else if (pname == "sz") spanstyle += "font-size:" + QString::number(val.toInt()/2) + "pt;";
	       else if (pname == "color") spanstyle += "color:#" + val + ";"; // hopefully hexa
	       else if (pname == "rFonts") {
                   QString family = stream.attributes().value("w:ascii").toString();
                   if (family == "Calibri") family = "Open Sans";
		   else if (family == "Calibri Light") family = "Open Sans Light";
                   else if (family == "Arial") family = "Roboto";

                   QString familyTheme = stream.attributes().value("w:asciiTheme").toString();

		   if (familyTheme == "majorHAnsi") family = majorFont;
		   else if (familyTheme == "minorHAnsi") family = minorFont;
                   
		   if (family != "") spanstyle += "font-family:" + family + ";";
	       }

	       stream.skipCurrentElement();
	   } 

	   return spanstyle;
       }

       QString parseParagraphStyles(QXmlStreamReader& stream) {
           QString style = "";
           while (stream.readNextStartElement()) {
               QString name = stream.name().toString();
               QString val = stream.attributes().value("w:val").toString();

               if (stream.name() == "rPr") style += parseLocalStyles(stream);
	       /*else if (stream.name() == "spacing") style += "line-height:"+val+"%;";
	       else if (stream.name() == "ind") {
                   QString lft = stream.attributes().value("w:left").toString();
		   QString rgt = stream.attributes().value("w:right").toString();

                   if (lft != "") style += "margin-left:"+lft+";"; 
		   if (rgt != "") style += "margin-right"+rgt+";";
	       }*/ else if (stream.name() == "pStyle") {
                   style += styleList[val];
               } 
	       
	       if (name != "rPr") stream.skipCurrentElement();
           }

	   return style;
       }

       QString convertMSDocument(QString filename) {
           KZip zdoc(filename);
           if (!zdoc.open(QIODevice::ReadOnly)) // read document
               return QString();

	   //KZipFileEntry* fontTables = dir->file("word/fontTable.xml");
	   //KZipFileEntry* styles     = dir->file("word/styles.xml");

	   if (zdoc.directory()->file("word/theme/theme1.xml")) {
               QIODevice* themeDevice = zdoc.directory()->file("word/theme/theme1.xml")->createDevice();
               QXmlStreamReader tstream(themeDevice);
               while (tstream.readNextStartElement()) {
                   if (tstream.name() == "theme") {
                        while (tstream.readNextStartElement()) {
                            if (tstream.name() == "themeElements") {
                                while (tstream.readNextStartElement()) {
                                    if (tstream.name() == "fontScheme") {
                                        while (tstream.readNextStartElement()) {
                                            if (tstream.name() == "majorFont" or tstream.name() == "minorFont") {
                                                QString whichFont = tstream.name().toString();
                                                while (tstream.readNextStartElement()) {
                                                    if (tstream.name() == "latin") {
                                                        QString themeFont = tstream.attributes().value("typeface").toString();
							if (whichFont == "majorFont") majorFont = themeFont;
							else if (whichFont == "minorFont") minorFont = themeFont;
						    } else tstream.skipCurrentElement();
						}
					    } else tstream.skipCurrentElement();
					}
				    } else tstream.skipCurrentElement();
				}
			    } else tstream.skipCurrentElement();
			}
		   } else tstream.skipCurrentElement();
	       }
	   }

           QIODevice* styleDevice = zdoc.directory()->file("word/styles.xml")->createDevice();
           QXmlStreamReader sstream(styleDevice);

           while (sstream.readNextStartElement()) {
               if (sstream.name() == "styles") {
                   while (sstream.readNextStartElement()) {
                       if (sstream.name() == "style") {
                           QString stylename = sstream.attributes().value("w:styleId").toString();
                           QString spanstyle = "";
                           while (sstream.readNextStartElement()) {
                               QString name = sstream.name().toString();

                               if (name == "rPr") spanstyle = parseLocalStyles(sstream);
			       else sstream.skipCurrentElement();
                           }

                         styleList[stylename] = spanstyle;
		       } else sstream.skipCurrentElement();
		   }
	       } else sstream.skipCurrentElement();
	   }

	   // sometimes Word and Word Online create a document2.xml
	   QString documentlocation = "word/document.xml";
	   if (zdoc.directory()->file(documentlocation) == NULL) documentlocation = "word/document2.xml";

           QIODevice* docDevice = zdoc.directory()->file(documentlocation)->createDevice();

	   QXmlStreamReader dstream(docDevice);

           QString html = "";

           html += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd\">"
                   "<html><head><meta name=\"qrichtext\" content=\"1\" />"
		   "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><style type=\"text/css\">"
                   "p, li { white-space: pre-wrap; }"
                   "</style></head><body style=\" font-family:'Open Sans'; font-size:10pt; font-weight:400; font-style:normal;\">"
		   "<table style=\"-qt-table-type: root; margin-top:48px; margin-bottom:48px; margin-left:48px; margin-right:48px;\">"
		   "<tr><td style=\"border: none;\">";

	   while (dstream.readNextStartElement()) {
               if (dstream.name() == "document") {
                   while (dstream.readNextStartElement()) {
                       if (dstream.name() == "body") {
                           while (dstream.readNextStartElement()) {
                               if (dstream.name() == "p") {
                                   QString style="";
				   QString text = "";
                                   while (dstream.readNextStartElement()) {
                                       if (dstream.name() == "r") {
                                           QString spanstyle = "";
                                           QString spantext = "";
                                           while (dstream.readNextStartElement()) {
                                               QString name = dstream.name().toString();

                                               if (name == "t") spantext += dstream.readElementText();
					       else if (name == "rPr") spanstyle += parseLocalStyles(dstream);
					       else {
                                                   if (name == "br") {
                                                       QString type = dstream.attributes().value("w:type").toString();
						       if (type == "page") spantext += "<br/>", style = "-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; page-break-before:always;";
                                                       else spantext += "<br/>";
						   }

					           dstream.skipCurrentElement();
					       }
				            }

                                            if (spanstyle != "") text += "<span style='"+spanstyle+"'>" + spantext + "</span>";
					    else text += spantext;
                                       } else if (dstream.name() == "pPr") style += parseParagraphStyles(dstream);
			               else dstream.skipCurrentElement();
			           }

				   if (style == "") html += "<p>" + text + "</p>";
				   else html += "<p style='"+style+"'>" + text + "</p>";
                               } else dstream.skipCurrentElement();
                           }
                       } else dstream.skipCurrentElement();
		   }
	       } else dstream.skipCurrentElement();
	   }

	   html += "</td></tr></table></body></html>";
	   //qDebug() << html;
           zdoc.close();
	   return html;
       }
   }
}
