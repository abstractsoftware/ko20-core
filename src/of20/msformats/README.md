# O20 Component MSFORMATS
A library used by o20 (o20.word) for reading and writing zipped and XML based document formats. The ODT import filter is based on one found in the KDE Okular source code, while the DOCX import is written from scratch. Both require KF5Archive and Qt5 (Core, XML).
