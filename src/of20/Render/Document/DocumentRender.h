/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef AWPAGEEDIT_HEADER
#define AWPAGEEDIT_HEADER

#include <KRichTextEdit>
#include <QTextBrowser>

class DocumentRender : public KRichTextEdit
{
    Q_OBJECT
public:
    explicit DocumentRender(QWidget* parent = 0);

    void setPageMargins(const QMargins& _margins);
    void setPageMarginsInch(qreal left, qreal top, qreal right, qreal bottom);

    void setBackgroundColor(const QString& _color = "#f0f0f0", const QString& _tcolor = "#000000");
    void setPageBorderColor(const QString& _color = "#c6c6c6");

    void setPageColor(const QColor& _color = Qt::white);
    void setPageSize(const QSizeF& _size);
    void setPageSizeInch(qreal width, qreal height);

    void SetFile();

    QMargins getPageMargins() { return m_margins; }
    QSizeF getPageSize() {return QSizeF(m_pageWidth, m_pageHeight);}
    int currentPageNumber();

    bool usePageMode() const;
    bool isPageContinuous() { return m_continuousPage; }
    void setPageContinuous(bool oo) { m_continuousPage = oo; }

    QMargins viewportMargins() { return KRichTextEdit::viewportMargins(); }
public slots:

    void setUsePageMode(bool _use);

    void setAddSpaceToBottom(bool _addSpace);

    void setShowPageNumbers(bool _show);

    void setPageNumbersAlignment(Qt::Alignment _align);

    void incFontSize() {
        setFontPointSize(fontPointSize() + 2);
    }

    void decFontSize() {
        setFontPointSize(fontPointSize() - 2);
    }

protected:
    void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);

    bool canInsertFromMimeData(const QMimeData *source);
    void insertFromMimeData(const QMimeData *source);
    void paintEvent(QPaintEvent* _event);
    void resizeEvent(QResizeEvent* _event);

public slots:
    void zoomIn(qreal zoomFactor = 1.5) {
        if (amountZoomed >= 10) return;
        m_pageWidth = m_pageWidth*zoomFactor;
        m_pageHeight = m_pageHeight*zoomFactor;

        QTextEdit::zoomIn(10);
        repaint();
        amountZoomed += 10;
    }

    void zoomOut(qreal zoomFactor = 1.5) {
        if (amountZoomed <= -10) return;
        m_pageWidth = m_pageWidth/zoomFactor;
        m_pageHeight = m_pageHeight/zoomFactor;

        QTextEdit::zoomOut(10);
        repaint();
        amountZoomed -= 10;
    }

/*
    void setFontPointSize(qreal fontSize) {
        QFont f = font();
        f.setPointSize(fontSize);
        setFont(f);
    } */

    //qreal fontPointSize() { return font().pointSize()-amountZoomed; }

private:
    void updateViewportMargins();

    void updateVerticalScrollRange();

    void paintPagesView();

    void paintPageNumbers();

    void paintPageNumber(QPainter* _painter, const QRectF& _rect, bool _isHeader, int _number);

    void insertImage(const QUrl& url, const QImage& image);
    void insertExternalText(const QUrl& url);

private slots:

    void aboutVerticalScrollRangeChanged(int _minimum, int _maximum);

    void aboutDocumentChanged();

    void aboutUpdateDocumentGeometry();

private:
    bool m_continuousPage;

    qreal amountZoomed = 0;

    QString m_filename;
    QString m_suffix;

    QTextDocument* m_document;

    QColor m_pageColor;
    QString m_backgroundColor;
    QString m_borderColor;

    qreal m_pageWidth;
    qreal m_pageHeight;

    QMargins m_margins;

    bool m_usePageMode;

    bool m_addBottomSpace;

    bool m_showPageNumbers;

    Qt::Alignment m_pageNumbersAlignment;

};

#endif
