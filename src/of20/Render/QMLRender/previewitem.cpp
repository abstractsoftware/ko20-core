/*
 * Copyright © 2003-2007 Fredrik Höglund <fredrik@kde.org>
 * Copyright 2019 Kai Uwe Broulik <kde@broulik.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "previewitem.h"

#include <QApplication>
#include <QHoverEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QContextMenuEvent>
#include <QKeyEvent>
#include <QWheelEvent>
#include <QPixmapCache>
#include <QQuickWindow>
#include <QStyleFactory>
#include <QWidget>
#include <QStyle>

//#include <KColorScheme>
//#include <KSharedConfig>

PreviewItem::PreviewItem(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    m_widget = new DocumentRender;
    // Don't actually show the widget as a separate window when calling show()
    m_widget->setAttribute(Qt::WA_DontShowOnScreen);
    // Do not wait for this widget to close before the app closes
    m_widget->setAttribute(Qt::WA_QuitOnClose, false);

    setAcceptHoverEvents(true);
    setFocus(true);

    //setFlag(QQuickItem::ItemAcceptsInputMethod, true);
    //setFlag(QQuickItem::ItemIsFocusScope, true);
    setAcceptedMouseButtons(Qt::AllButtons);
    setFlag(QQuickItem::ItemAcceptsDrops, true);
    //setFocusPolicy(Qt::StrongFocus);

    // HACK QtCurve deadlocks on application teardown when the Q_GLOBAL_STATIC QFactoryLoader
    // in QStyleFactory is destroyed which destroys all loaded styles prompting QtCurve
    // to disconnect from DBus stalling the application.
    // This also happens before any of the KCM objects are destroyed, so our only chance
    // is cleaning up in response to aboutToQuit
    connect(qApp, &QApplication::aboutToQuit, this, [this] {
        m_style.reset();
    });
}

PreviewItem::~PreviewItem() = default;

DocumentRender* PreviewItem::getDocument() { return m_widget; }

void PreviewItem::componentComplete()
{
    QQuickPaintedItem::componentComplete();
    reload();
}

bool PreviewItem::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == m_widget) {
        switch (event->type()) {
        case QEvent::Show:
        case QEvent::UpdateRequest:
            update();
            break;
        default:
            break;
        }
    }

    return QQuickPaintedItem::eventFilter(watched, event);
}

QString PreviewItem::styleName() const
{
    return m_styleName;
}

void PreviewItem::setStyleName(const QString &styleName)
{
    if (m_styleName == styleName) {
        return;
    }

    m_styleName = styleName;
    reload();
    emit styleNameChanged();
}

bool PreviewItem::isValid() const
{
    return m_style && m_widget;
}

void setStyleRecursively(QWidget *widget, QStyle *style, const QPalette &palette)
{
    // Don't let styles kill the palette for other styles being previewed.
    widget->setPalette(QPalette());

    widget->setPalette(palette);

    widget->setStyle(style);

    const auto children = widget->children();
    for (QObject *child : children) {
        if (child->isWidgetType()) {
            setStyleRecursively(static_cast<QWidget *>(child), style, palette);
        }
    }
}

void PreviewItem::reload()
{
    if (!isComponentComplete()) {
        return;
    }

    const bool oldValid = isValid();

    m_style.reset(QStyleFactory::create("fusion"));
    if (!m_style) {
        qWarning() << "Failed to load style" << m_styleName;
        if (oldValid != isValid()) {
            emit validChanged();
        }
        return;
    }

    // XXX m_widget = new DocumentRender;
    //m_ui.setupUi(m_widget);

    // Prevent Qt from wrongly caching radio button images
    QPixmapCache::clear();

    QPalette palette = qApp->palette();
    //QPalette palette(KColorScheme::createApplicationPalette(KSharedConfig::openConfig()));
    m_style->polish(palette);

    // HACK Needed so the previews look like their window is active
    // The previews don't have a parent (we're in QML, after all, there is no QWidget* to parent it to)
    // so QWidget::isActiveWindow() always returns false making the widget look dull
    // You still won't get hover effects in some themes (those that don't do that for inactive windows)
    // but at least at a glance it looks fine...
    for (int i = 0; i < QPalette::NColorRoles; ++i) {
        const auto role = static_cast<QPalette::ColorRole>(i);
        palette.setColor(QPalette::Inactive, role, palette.color(QPalette::Active, role));
    }

//    setStyleRecursively(m_widget, m_style.data(), palette);

    m_widget->ensurePolished();

    m_widget->resize(qRound(width()), qRound(height()));

    m_widget->installEventFilter(this);

    m_widget->show();

    const auto sizeHint = m_widget->sizeHint();
    setImplicitSize(sizeHint.width(), sizeHint.height());

    if (oldValid != isValid()) {
        emit validChanged();
    }
}

void PreviewItem::paint(QPainter *painter)
{
    if (m_widget && m_widget->isVisible()) {
        m_widget->render(painter);
//	painter->scale(200,200);
    }
}

void PreviewItem::hoverEnterEvent(QHoverEvent* event) {
        //QWidget *child = m_widget->childAt(event->pos());
        //QApplication::sendEvent(child ? child : m_widget, event);
	//forceActiveFocus();
}

void PreviewItem::hoverMoveEvent(QHoverEvent *event)
{
    return;
    sendHoverEvent(event);
}

void PreviewItem::mousePressEvent(QMouseEvent* event)
{
//	qDebug() << "mouse press" << m_widget->childAt(event->pos()) << event->pos() << event->globalPos() << mapToGlobal(event->pos());

	QWidget *child = m_widget->childAt(event->pos());

	QMargins margins = m_widget->viewportMargins();
        QPointF modifiedPoint = event->pos();
	modifiedPoint.setX(modifiedPoint.x() - margins.left());
	modifiedPoint.setY(modifiedPoint.y() - margins.top());
	QMouseEvent *e = new QMouseEvent(QEvent::MouseButtonPress, modifiedPoint, event->button(), event->buttons(), event->modifiers());
        QApplication::sendEvent(child ? child : m_widget, e);

	if (event->button() == Qt::RightButton)
	{
		//qDebug() << "right";

		QContextMenuEvent* contextEvent = new QContextMenuEvent(QContextMenuEvent::Mouse, 
				modifiedPoint.toPoint(), mapToGlobal(event->pos()).toPoint(), QApplication::keyboardModifiers());

		QApplication::sendEvent(child ? child : m_widget, contextEvent);
	}	

	forceActiveFocus();
	//(child ? child : m_widget)->setFocus();
	
	//event->accept();
}

void PreviewItem::mouseMoveEvent(QMouseEvent* event)
{
	//qDebug() << "mouse move event!";

	QWidget *child = m_widget->childAt(event->pos());
	//qDebug() << child;
        QMargins margins = m_widget->viewportMargins();
        QPointF modifiedPoint = event->pos();
        modifiedPoint.setX(modifiedPoint.x() - margins.left());
	modifiedPoint.setY(modifiedPoint.y() - margins.top());
        QMouseEvent *e = new QMouseEvent(QEvent::MouseMove, modifiedPoint, event->button(), event->buttons(), event->modifiers());
        QApplication::sendEvent(child ? child : m_widget, e);
}

void PreviewItem::hoverLeaveEvent(QHoverEvent *event)
{
    if (m_lastWidgetUnderMouse) {
        dispatchEnterLeave(nullptr, m_lastWidgetUnderMouse, mapToGlobal(event->pos()));
        m_lastWidgetUnderMouse = nullptr;
    }
}

void PreviewItem::sendHoverEvent(QHoverEvent *event)
{
    if (!m_widget || !m_widget->isVisible()) {
        return;
    }

    QWidget* old_lastWidgetUnderMouse = m_lastWidgetUnderMouse;

    QPointF pos = event->pos();

    QWidget *child = m_widget->childAt(pos.toPoint());
    QWidget *receiver = child ? child : m_widget;

    if (m_lastWidgetUnderMouse != m_widget)
    	dispatchEnterLeave(receiver, m_lastWidgetUnderMouse, mapToGlobal(event->pos()));

    m_lastWidgetUnderMouse = m_widget;

    pos = receiver->mapFrom(m_widget, pos.toPoint());

    QMouseEvent mouseEvent(QEvent::MouseMove, pos, receiver->mapTo(receiver->topLevelWidget(), pos.toPoint()),
                           receiver->mapToGlobal(pos.toPoint()),
                           Qt::NoButton, {}, event->modifiers());

    qApp->sendEvent(receiver, &mouseEvent);

    event->setAccepted(mouseEvent.isAccepted());
}

// Simplified copy of QApplicationPrivate::dispatchEnterLeave
void PreviewItem::dispatchEnterLeave(QWidget *enter, QWidget *leave, const QPointF &globalPosF)
{
    if ((!enter && !leave) || (enter == leave)) {
        return;
    }

    QWidgetList leaveList;
    QWidgetList enterList;

    bool sameWindow = leave && enter && leave->window() == enter->window();
    if (leave && !sameWindow) {
        auto *w = leave;
        do {
            leaveList.append(w);
        } while (!w->isWindow() && (w = w->parentWidget()));
    }
    if (enter && !sameWindow) {
        auto *w = enter;
        do {
            enterList.append(w);
        } while (!w->isWindow() && (w = w->parentWidget()));
    }
    if (sameWindow) {
        int enterDepth = 0;
        int leaveDepth = 0;
        auto *e = enter;
        while (!e->isWindow() && (e = e->parentWidget()))
            enterDepth++;
        auto *l = leave;
        while (!l->isWindow() && (l = l->parentWidget()))
            leaveDepth++;
        QWidget* wenter = enter;
        QWidget* wleave = leave;
        while (enterDepth > leaveDepth) {
            wenter = wenter->parentWidget();
            enterDepth--;
        }
        while (leaveDepth > enterDepth) {
            wleave = wleave->parentWidget();
            leaveDepth--;
        }
        while (!wenter->isWindow() && wenter != wleave) {
            wenter = wenter->parentWidget();
            wleave = wleave->parentWidget();
        }

        for (auto *w = leave; w != wleave; w = w->parentWidget())
            leaveList.append(w);

        for (auto *w = enter; w != wenter; w = w->parentWidget())
            enterList.append(w);
    }

    const QPoint globalPos = globalPosF.toPoint();

    QEvent leaveEvent(QEvent::Leave);
    for (int i = 0; i < leaveList.size(); ++i) {
        auto *w = leaveList.at(i);
        QApplication::sendEvent(w, &leaveEvent);
        if (w->testAttribute(Qt::WA_Hover)) {
            QHoverEvent he(QEvent::HoverLeave, QPoint(-1, -1), w->mapFromGlobal(globalPos),
                            QApplication::keyboardModifiers());
            QApplication::sendEvent(w, &he);
        }
    }
    if (!enterList.isEmpty()) {
        const QPoint windowPos = qAsConst(enterList).back()->window()->mapFromGlobal(globalPos);
        for (auto it = enterList.crbegin(), end = enterList.crend(); it != end; ++it) {
            auto *w = *it;
            const QPointF localPos = w->mapFromGlobal(globalPos);
            QEnterEvent enterEvent(localPos, windowPos, globalPosF);
            QApplication::sendEvent(w, &enterEvent);
            if (w->testAttribute(Qt::WA_Hover)) {
                QHoverEvent he(QEvent::HoverEnter, localPos, QPoint(-1, -1),
                               QApplication::keyboardModifiers());
                QApplication::sendEvent(w, &he);
            }
        }
    }
}

void PreviewItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (m_widget && newGeometry != oldGeometry) {
        m_widget->resize(qRound(newGeometry.width()), qRound(newGeometry.height()));
    }

    QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);
}

void PreviewItem::keyPressEvent(QKeyEvent* event)
{
	//QKeyEvent* key = new QKeyEvent(QEvent::KeyPress, 0, QApplication::keyboardModifiers(), "gee");
//	qDebug() << "XXX key press" << event->text();
	//m_widget->setFocus();
	QApplication::sendEvent(m_widget, event);
}

void PreviewItem::keyReleaseEvent(QKeyEvent* event)
{
	//qDebug() << "XXX key release";
	//m_widget->setFocus();
	QApplication::sendEvent(m_widget, event);
}

void PreviewItem::mouseReleaseEvent(QMouseEvent* event)
{
	QWidget *child = m_widget->childAt(event->pos());
        QMargins margins = m_widget->viewportMargins();
        QPointF modifiedPoint = event->pos();
        modifiedPoint.setX(modifiedPoint.x() - margins.left());
        modifiedPoint.setY(modifiedPoint.y() - margins.top());
        QMouseEvent *e = new QMouseEvent(QEvent::MouseButtonRelease, modifiedPoint, event->button(), event->buttons(), event->modifiers());
        QApplication::sendEvent(child ? child : m_widget, e);
}

bool PreviewItem::event(QEvent* event)
{
	if (event->type() != QEvent::HoverMove and event->type() != QEvent::KeyPress)
		QApplication::sendEvent(m_widget, event);
	return QQuickItem::event(event);
}

void PreviewItem::focusInEvent(QFocusEvent *event)
{
	//qDebug() << "XXX focus in";
        QApplication::sendEvent(m_widget, event);
	forceActiveFocus();
}

void PreviewItem::wheelEvent(QWheelEvent* event)
{
	//qDebug() << "wheel event";
	QWidget *child = m_widget->childAt(event->pos());
	QApplication::sendEvent(child ? child : m_widget, event);
}

void PreviewItem::mouseDoubleClickEvent(QMouseEvent* event)
{
	QWidget *child = m_widget->childAt(event->pos());

        QMargins margins = m_widget->viewportMargins();
        QPointF modifiedPoint = event->pos();
        modifiedPoint.setX(modifiedPoint.x() - margins.left());
        modifiedPoint.setY(modifiedPoint.y() - margins.top());
        QMouseEvent *e = new QMouseEvent(QEvent::MouseButtonDblClick, modifiedPoint, event->button(), event->buttons(), event->modifiers());
	QApplication::sendEvent(child ? child : m_widget, e);
}

void PreviewItem::timerEvent(QTimerEvent* event)
{
	//qDebug() << "timer event";
	QApplication::sendEvent(m_widget, event);
}

void PreviewItem::dragEnterEvent(QDragEnterEvent *event)
{
	qDebug() << "XXX drag enter event!";
}
