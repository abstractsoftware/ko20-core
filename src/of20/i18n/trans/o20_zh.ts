<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>O20::Word</name>
    <message>
        <location filename="../../../Word/Word.cpp" line="266"/>
        <source>%1 %2   %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Word/Word.cpp" line="269"/>
        <source>%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>O20HomePage</name>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="40"/>
        <source>Hello.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="40"/>
        <source>Hi!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="40"/>
        <source>Let&apos;s get started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="40"/>
        <source>Welcome.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="40"/>
        <source>Greetings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="52"/>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="130"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="58"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="131"/>
        <source>Blank Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="150"/>
        <source>Single Spaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="151"/>
        <source>General Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="152"/>
        <source>Basic Blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="153"/>
        <source>Business Letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="154"/>
        <source>Bold Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="155"/>
        <source>Student Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="156"/>
        <source>Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="157"/>
        <source>Resume Cover Letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="166"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="174"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="180"/>
        <source>Import Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="186"/>
        <source>Display Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="239"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="244"/>
        <source>Save As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="250"/>
        <source>Export to PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="257"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="266"/>
        <source>Rename document to...?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="282"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="289"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="302"/>
        <source>About Kraken Office</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="344"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="356"/>
        <source>About Us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="366"/>
        <source>Develop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="387"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="410"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="413"/>
        <source>Recover the window state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="414"/>
        <source>Show the splash screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="415"/>
        <source>Show the start screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="429"/>
        <source>User Customization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="432"/>
        <source>What would you like me to call you?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="433"/>
        <source>What is your full name?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="434"/>
        <source>What are your initials?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="437"/>
        <source>Use Dark Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20HomePage.qml" line="438"/>
        <source>useDarkMode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>O20Ribbon</name>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20Ribbon.qml" line="65"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20Ribbon.qml" line="66"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Resource/QtQuick/O20/O20Ribbon.qml" line="67"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
