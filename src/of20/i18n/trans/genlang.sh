FLIST="../../../Word/Word.cpp ../../../Resource/QtQuick/O20/O20HomePage.qml ../../../Resource/QtQuick/O20/O20Ribbon.qml"
lupdate $FLIST -ts o20_en.ts -target-language en
lupdate $FLIST -ts o20_es.ts -target-language es
lupdate $FLIST -ts o20_fr.ts -target-language fr
lupdate $FLIST -ts o20_de.ts -target-language de
lupdate $FLIST -ts o20_ru.ts -target-language ru
lupdate $FLIST -ts o20_zh.ts -target-language zh
lupdate $FLIST -ts o20_pt.ts -target-language pt
lupdate $FLIST -ts o20_hi.ts -target-language hi
lupdate $FLIST -ts o20_ja.ts -target-language ja
lupdate $FLIST -ts o20_it.ts -target-language it
lupdate $FLIST -ts o20_cs.ts -target-language cs
lupdate $FLIST -ts o20_el.ts -target-language el
