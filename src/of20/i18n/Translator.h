#ifndef O20TRANSLATOR_H
#define O20TRANSLATOR_H

#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

namespace O20 {
	class Translator {
		public:
			Translator() {
				transQt.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
				transO20.load(":/src/o20_" + QLocale::system().name() + ".qm");
			}

			void apply(QApplication* app) {
				app->installTranslator(&transO20);
				app->installTranslator(&transQt);
			}

		private:
			QTranslator transQt;
			QTranslator transO20;
	};
};

#endif
