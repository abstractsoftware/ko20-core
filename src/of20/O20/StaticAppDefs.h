#pragma once

#include "App.h"

namespace KO20 {

    App WordApp(
        "KO20.Ink",
        "qrc:/O20Core/apps/io.gitlab.ko20.ink.svg",
        "#2b579a"
    );

/*    App SlideShowApp(
        "O20.SlideShow",
        "qrc:/O20Core/apps/ms-powerpoint.svg",
        "#b7472a"
    );

    App NotebookApp(
        "O20.NoteBook",  
        "qrc:/O20Core/apps/ms-onenote.svg",
        "#7719aa"
    );*/
}
