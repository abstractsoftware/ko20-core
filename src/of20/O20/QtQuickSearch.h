#pragma once

#include <QQmlProperty>

namespace O20 {

template <class T>
T findChild(QQuickItem* object, const QString& objectName)
{
        QList<QQuickItem*> children = object->childItems();
        for (QQuickItem* item : children)
        {
            if (QQmlProperty::read(item, "objectName").toString() == objectName)
                return item;

            T child = findChild<QQuickItem*>(item, objectName);

            if (child)
                return child;
    }

    return nullptr;
}

/*
template <class T>
T findChild(QQuickItem* object, const QString& objectName)
{
        QList<QQuickItem*> children = object->childItems();
        foreach (QQuickItem* item, children)
        {
		qDebug() << QQmlProperty::read(item, "objectName").toString();
            if (QQmlProperty::read(item, "objectName").toString() == objectName)
                return item;

            T child = findChild<QQuickItem*>(item, objectName);

            if (child)
                return child;
    }
    return nullptr;
}*/

}
