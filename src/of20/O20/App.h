#ifndef O20APPDATA_H
#define O20APPDATA_H

namespace KO20 {

	struct App {
		App() {};
		App(QString a, QString b, QString c):
			name(a), icon(b), color(c) {};

		QString name = "Kraken Office";
		QString icon = "qrc:/O20Core/apps/ms-office.svg";
		QString color = "#d83b01";
	};

}

#endif
