#ifndef QQUICKBIND_HEADER
#define QQUICKBIND_HEADER

/* QtQuick access member functions */

#define O20_QO(a) rootObject()->findChild<QObject*>(a)
#define O20_QOGET(a, b) O20_QO(a)->property(b)
#define O20_QOSET(a, b, c) O20_QO(a)->setProperty(b, c)

#endif
