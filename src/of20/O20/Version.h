#ifndef O20DECLS_HEADER
#define O20DECLS_HEADER

#define O20_VERSION "20.3.0"
#define O20_EDITION (qEnvironmentVariableIsSet("SNAP") ? "FLATPAK" : "SNAPCRAFT")

#endif
