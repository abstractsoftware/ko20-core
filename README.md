# KrakenOffice 20 (KO20)

(C) Abstract Software Project 2019-2020
KO20 is the second generation version of O20, written in the Qt Quick scripting language. It utilises the OfficeFramework X and QuickDesign, on a small and lightweight code base.

## About KO20

The new KO20 office suite written in QtQuick 2.0 with a modern MS office design, Google Material controls, floating toolbars, ODT support, syntax highlighting for devs in over 100 programming languages, and more! This is an evolution of the O20 office suite. Our word processor and MS Word alternative, Ink, is perfect for developing, drafting novels, and writing simple essays.

Developed on ArchLinux for KDE Plasma

<img src='pics/kdeapp.svg'/><img src='pics/distributor-logo-archlinux.svg'>

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/ko20)
